#########################################################
# E12Lib - priority                                     #
# Author: Antonio Horta <ajhorta@cybercrafter.com.br>   #
# https://gitlab.com/cybercrafter/e12lib                #
# Cybercrafter ® 2021                                   #
#########################################################

import os
from .constant import *
from .kdd import KDDSec
from stix2 import Filter, FileSystemSource
import pandas as pd
import numpy as np
import requests
from scikitmcda.topsis import TOPSIS 
from scikitmcda.vikor import VIKOR
from scikitmcda.waspas import WASPAS
from scikitmcda.wpm import WPM
from scikitmcda.wsm import WSM
from scikitmcda.electre_ii import ELECTRE_II
from scikitmcda.promethee_ii import PROMETHEE_II
from scikitmcda.ensemble import ENSEMBLE 

class Priority:
    def __init__(self):
        self.cves: []
        self.capecs: []
        self.weights: []
        self.criteria: []
        self.dataframe: None
        self.weights_likelihood: []
        self.criteria_likelihood: []
        self.dataframe_likelihood: None
        self.weights_impact: []
        self.criteria_impact: []
        self.dataframe_impact: None
        self.df_decision: None
        self.method: 0

    def ranking_by_chain_risk(self, cve_chaining_list, mcda="TOPSIS", alt_labels=None, impact_ahp=None, impact_ranking=0):
        """
        Rank by risk of CVEs chaining found in each system. 
        It prioritizes risk by the probability (exploitability) 
        and by impact scores extracted from CVE.

        :param cve_chaining_list: list of cve ids chaining for ranking
        :type cve_chaining_list: nested lists [[]]

        :param mcda: Algorithm for ranking. Default is "TOPSIS" but accept "VIKOR", "PROMETHEE_II", "WASPAS", "WSM", "WPM", "ELECTRE_II" too.   
        :type mcda: list

        :param alt_labels: Labels of alternatives
        :type alt_labels: list

        :param impact_ahp: Matrix AHP defining impact of alternatives
        :type impact_ahp: list

        :param impact_ranking: Rank attribute method for determinig weight (impact). Default=0 accept 1 up to 4 
        :type impact_ranking: Integer

        :return: rank decision dataframe
        :rtype: pandas dataframe
        """

        if mcda == "VIKOR":
            dm_l = VIKOR()
            dm_i = VIKOR()
        elif mcda == "PROMETHEE_II":
            dm_l = PROMETHEE_II()
            dm_i = PROMETHEE_II()
        elif mcda == "WASPAS":
            dm_l = WASPAS()
            dm_i = WASPAS()
        elif mcda == "WSM":
            dm_l = WSM()
            dm_i = WSM()
        elif mcda == "WPM":
            dm_l = WPM()
            dm_i = WPM()
        elif mcda == "ELECTRE_II":
            dm_l = ELECTRE_II()
            dm_i = ELECTRE_II()
        else:
            dm_l = TOPSIS()
            dm_i = TOPSIS()

        self.__likelihood_maker(cve_chaining_list)
        self.__impact_maker(cve_chaining_list)
        
        # proceed likelihood dm
        if alt_labels != None and type(alt_labels) == list and len(alt_labels) == self.dataframe_likelihood.shape[0]: 
            dm_l.dataframe(self.dataframe_likelihood.values, alt_labels)
        else:
            dm_l.dataframe(self.dataframe_likelihood.values)
        dm_l.set_signals([1] * len(self.criteria_likelihood))
        dm_l.set_weights_manually(self.weights_likelihood, True)
        dm_l.decide()


        self.df_decision = pd.DataFrame(dm_l.df_original.iloc[:, 0])
        self.df_decision["likelihood"] = dm_l.df_decision.iloc[:,-2]

        # proceed impact dm
        if impact_ahp is None and impact_ranking == 0:
            if alt_labels != None and type(alt_labels) == list and len(alt_labels) == self.dataframe_impact.shape[0]: 
                dm_i.dataframe(self.dataframe_impact.values, alt_labels)
            else:
                dm_i.dataframe(self.dataframe_impact.values)
            dm_i.set_signals([1] * len(self.criteria_impact))
            dm_i.set_weights_manually(self.weights_impact, True)
            dm_i.decide()
            self.df_decision["impact"] = dm_i.df_decision.iloc[:,-2] 
            self.df_decision["risk"] = dm_l.df_decision.iloc[:,-2] * dm_i.df_decision.iloc[:,-2]
        
        elif impact_ranking > 0 and impact_ahp is None:
            if alt_labels != None and type(alt_labels) == list: 
                dm_i.dataframe( np.zeros((self.df_decision.shape[0], self.df_decision.shape[0])), alt_labels)
            else:
                dm_i.dataframe(np.zeros((self.df_decision.shape[0], self.df_decision.shape[0])))
            dm_i.set_signals([1] * self.df_decision.shape[0])
            if impact_ranking == 1:
                dm_i.set_weights_by_ranking_A()
            elif impact_ranking == 2:
                dm_i.set_weights_by_ranking_B()
            elif impact_ranking == 3:
                dm_i.set_weights_by_ranking_B_POW()
            elif impact_ranking == 4:
                dm_i.set_weights_by_ranking_C()
            self.df_decision["impact"] = dm_i.weights 
            self.df_decision["risk"] = dm_l.df_decision.iloc[:,-2] * dm_i.weights
        else:
            if alt_labels != None and type(alt_labels) == list and len(alt_labels) == len(impact_ahp[0]): 
                dm_i.dataframe(impact_ahp, alt_labels)
            else:
                dm_i.dataframe(impact_ahp)
            dm_i.set_signals([1] * len(impact_ahp[0]))
            ahp = dm_i.set_weights_by_AHP(impact_ahp)
            if ahp["consistency"] is False:
                print("AHP Return: ", ahp)
                raise("AHP Fail...:-(")
            self.df_decision["impact"] = dm_i.weights 
            self.df_decision["risk"] = dm_l.df_decision.iloc[:,-2] * dm_i.weights
            
        
        i = np.arange(1, len(self.df_decision.index)+1, 1)
        df_ranking = self.df_decision.sort_values(by=["risk"], ascending=False)
        df_ranking["rank"] = i
        
        self.df_decision = df_ranking.sort_index()

        self.method = dm_l        
        self.method.df_decision = self.df_decision  # setting correct df for voting

        return self.df_decision

    def ranking_by_chain_severity(self, chaining_list, mcda="TOPSIS", alt_labels=None):
        """
        Rank by severity a list of cve chaining through CVEs/CAPEC found in each system, APT or attack

        :param chaining_list: list of cve ids chaining for ranking
        :type chaining_list: nested lists [[]]

        :param mcda: Algorithm for ranking. Default is "TOPSIS" but accept "VIKOR", "PROMETHEE_II", "WASPAS", "WSM", "WPM", "ELECTRE_II" too.   
        :type mcda: list

        :param alt_labels: Labels of alternatives
        :type alt_labels: list

        :return: rank decision dataframe
        :rtype: pandas dataframe
        """

        self.__mcda_maker(chaining_list)

        if mcda == "VIKOR":
            dm = VIKOR()
        elif mcda == "PROMETHEE_II":
            dm = PROMETHEE_II()
        elif mcda == "WASPAS":
            dm = WASPAS()
        elif mcda == "WSM":
            dm = WSM()
        elif mcda == "WPM":
            dm = WPM()
        elif mcda == "ELECTRE_II":
            dm = ELECTRE_II()
        else:
            dm = TOPSIS()

        if alt_labels != None and type(alt_labels) == list and len(alt_labels) == self.dataframe.shape[0]: 
            dm.dataframe(self.dataframe.values, alt_labels)
        else:
            dm.dataframe(self.dataframe.values)
        dm.set_signals([1] * len(self.criteria))
        dm.set_weights_manually(self.weights, True)
        dm.decide()

        self.df_decision = self.dataframe.join(dm.df_decision.iloc[:,-2:])        
        self.df_decision = pd.DataFrame(dm.df_original.iloc[:, 0]).join(self.df_decision)
        self.method = dm

        return self.df_decision

    def __get_cve_info_from_nvd(self, cves_list):
        """
        This method obtains information from https://services.nvd.nist.gov/rest/json/cve/1.0/ based on the list of CVEs reported
        and set "cves" attribute with the results

        :param cve: list of CVEs
        :type cve: list
        """
        self.cves = []

        for c in cves_list:
            url = "https://services.nvd.nist.gov/rest/json/cve/1.0/" + c

            response = requests.get(url)
            cve = c
            cvss3_score = 0
            cvss2_score = 0
            exploitabilityScore2 = 0
            impactScore2 = 0
            exploitabilityScore3 = 0
            impactScore3 = 0
            try:
                cvss2_score = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV2"]["cvssV2"]["baseScore"]
                cvss3_score = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV3"]["cvssV3"]["baseScore"]
                exploitabilityScore2 = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV2"]["exploitabilityScore"]
                impactScore2 = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV2"]["impactScore"]
                exploitabilityScore3 = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV3"]["exploitabilityScore"]
                impactScore3 = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV3"]["impactScore"]
            except Exception as e: 
                # cvss2_score = response.json()["result"]["CVE_Items"][0]["impact"]["baseMetricV2"]["cvssV2"]["baseScore"]
                if "message" in response.json(): 
                    cvss3_score = 0
                    cvss2_score = 0
                    exploitabilityScore2 = 0
                    impactScore2 = 0
                    exploitabilityScore3 = 0
                    impactScore3 = 0
                    print(response.json())
                
            self.cves.append({"CVE": cve, "cvss3_score": cvss3_score, "cvss_score": cvss2_score, "exploitabilityScore2": exploitabilityScore2, "impactScore2": impactScore2, "exploitabilityScore3": exploitabilityScore3, "impactScore3": impactScore3 })    

    def __get_capec_info_from_local(self, capec_list):
        """
        This method obtains information from downloaded CTI jsons based on the list of CAPECs
        and set "capecs" attribute with the results

        :param capec_list: list of CAPECs
        :type capec_list: list
        """
        # update repository
        kddsec = KDDSec()
        kddsec.update()
        
        # Set CAPEC dir
        repo_dir = os.path.join(os.path.dirname(__file__), REPO_FOLDER)  # get current directory
        tc_path = os.path.join(repo_dir, "capec")
        tc_source = FileSystemSource(tc_path)

        # Create filters to retrieve CAPEC content from local CTI
        filter_objs = {"techniques": Filter("type", "=", "attack-pattern"),
                       "mitigations": Filter("type", "=", "course-of-action"),
                       "relationships": Filter("type", "=", "relationship")
                       }

        # Attack Pattern update
        elements = tc_source.query(filter_objs["techniques"])
        techniques = self.__mitre_capec_elements_update(elements, "capec")

        self.capecs = []

        for c in capec_list:
            for t in techniques:
                if c == t["external_id"]:                    
                    self.capecs.append({"CAPEC": c, 
                                        "x_capec_typical_severity": t["x_capec_typical_severity"], 
                                        "severity_score": self.__convert_x_capec_typical_severity(t["x_capec_typical_severity"]), 
                                        "likelihood_of_attack": self.__convert_x_capec_likelihood_of_attack(t["x_capec_likelihood_of_attack"])
                                        })
                    break
    
    def __convert_x_capec_typical_severity(self, x_capec_typical_severity):
        if x_capec_typical_severity == None:
            return 0
        elif x_capec_typical_severity == "Low":
            return 2.5
        elif x_capec_typical_severity == "Medium":
            return 5
        elif x_capec_typical_severity == "High":
            return 7.5
        elif x_capec_typical_severity == "Very High":
            return 10
    
    def __convert_x_capec_likelihood_of_attack(self, x_capec_likelihood_of_attack):
        if x_capec_likelihood_of_attack == None:
            return 0
        elif x_capec_likelihood_of_attack == "Low":
            return 3.3
        elif x_capec_likelihood_of_attack == "Medium":
            return 6.6
        elif x_capec_likelihood_of_attack == "High":
            return 10
       
    def __mitre_capec_elements_update(self, elements, dirs):

        mitrecapecelements = []

        for i in elements:
            if 'description' in i:
                description = i.description
            else:
                description = None

            if 'x_capec_typical_severity' in i:
                x_capec_typical_severity = i.x_capec_typical_severity
            else:
                x_capec_typical_severity = None

            if 'x_capec_likelihood_of_attack' in i:
                x_capec_likelihood_of_attack = i.x_capec_likelihood_of_attack
            else:
                x_capec_likelihood_of_attack = None

            mitrecapecelement = {'source_repo': dirs,
                                 'mitre_capec_element_id': i.id,
                                 'mitre_capec_element_type': i.type,
                                 'name': i.name,
                                 'description': description,
                                 'x_capec_typical_severity': x_capec_typical_severity,
                                 'x_capec_likelihood_of_attack': x_capec_likelihood_of_attack
                                 }

            external_references = self.__external_references_update(i)
            mitrecapecelement["external_id"] = external_references[0]
            mitrecapecelement["external_references"] = external_references[1]

            mitrecapecelements.append(mitrecapecelement)

        return mitrecapecelements

    def __external_references_update(self, i):

        externalreferences = []

        if 'external_references' in i:
            external_id_ = ""
            for er in i.external_references:
                if 'url' in er:
                    url = er.url
                else:
                    url = None
                if 'description' in er:
                    description = er.description
                else:
                    description = None
                if 'external_id' in er:
                    if er.external_id.startswith('CAPEC-'):
                        external_id_ = er.external_id
                    external_id = er.external_id
                else:
                    external_id = None
                if 'source_name' in er:
                    externalreference = {'source_name': er.source_name,
                                         'url': url,
                                         'external_id': external_id,
                                         'description': description}

                    externalreferences.append(externalreference)

        return external_id_, externalreferences

    def __filter_distinct_threats(self, threats_chaining_list):
        threat_list = [] 
        if type(threats_chaining_list) == str: # if just a CVE/CAPEC by string
            threat_list.append(threats_chaining_list)
        elif type(threats_chaining_list) == list:
            for threat_id in threats_chaining_list:
                if type(threat_id) == list:
                    for id_ in threat_id:
                        threat_list.append(id_)                        
                else: 
                    threat_list.append(threat_id)

        distinct_threats_list = []
        for d in threat_list:
            if d not in distinct_threats_list:
                distinct_threats_list.append(d)

        distinct_cve_list = []
        distinct_capec_list = []

        for t in distinct_threats_list:
            if t.startswith("CVE-"):
                distinct_cve_list.append(t)
            elif t.startswith("CAPEC-"):
                distinct_capec_list.append(t)

        self.__get_cve_info_from_nvd(distinct_cve_list)
        self.__get_capec_info_from_local(distinct_capec_list)

    def __mcda_maker(self, threat_chaining_list):
        """
        Build performance matrix for decision
        """
        
        self.__filter_distinct_threats(threat_chaining_list)
        w = [] # list of weights based on cvss3_score of cve
        c = [] # criteria CVE_id/CAPEC_id

        for cve in self.cves:
            c.append(cve["CVE"])
            if "cvss3_score" in cve: 
                if cve["cvss3_score"] is not None and cve["cvss3_score"] > 0:
                    w.append(float(cve["cvss3_score"]))
                elif "cvss_score" in cve:
                    if cve["cvss_score"] is not None and cve["cvss_score"] > 0:
                        w.append(float(cve["cvss_score"]))
                    else:
                        w.append(0.0)
            elif "cvss_score" in cve:
                if cve["cvss_score"] is not None and cve["cvss_score"] > 0:
                    w.append(float(cve["cvss_score"]))
            else:
                w.append(0.0)

        for capec in self.capecs:
            c.append(capec["CAPEC"])
            if "severity_score" in capec: 
                if capec["severity_score"] is not None and capec["severity_score"] > 0:
                    w.append(float(capec["severity_score"]))
                else:
                    w.append(0.0)
            else:
                w.append(0.0)

        w = self.__scale_weights(w)            
        self.weights = w
        self.criteria = c
        labels = []
        for l in c: 
            labels.append(l + " W" + str(round(w[c.index(l)],2))) 



        df = pd.DataFrame(np.zeros((len(threat_chaining_list), len(self.criteria))), columns=labels) 

        for a in range(0, len(threat_chaining_list)):
            if type(threat_chaining_list) == str and threat_chaining_list in self.criteria: # if just a CAPEC/CVE by string
                df.iloc[a, self.criteria.index(threat_chaining_list)] = 1
            elif type(threat_chaining_list) == list:
                for threat_id in threat_chaining_list[a]:
                    if type(threat_id) == list:
                        for id_ in threat_id:
                            if id_ in self.criteria:
                                df.iloc[a, self.criteria.index(id_)] = 1                                                                           
                    elif threat_id in self.criteria: 
                        df.iloc[a, self.criteria.index(threat_id)] = 1    

        self.dataframe = df

    def __likelihood_maker(self, threat_chaining_list):
        """
        Build performance matrix for decision
        """
        
        self.__filter_distinct_threats(threat_chaining_list)
        w = [] # list of weights based on cvss3_score of cve
        c = [] # criteria CVE_id/CAPEC_id

        for cve in self.cves:
            c.append(cve["CVE"])
            if "exploitabilityScore3" in cve: 
                if cve["exploitabilityScore3"] is not None and cve["exploitabilityScore3"] > 0:
                    w.append(float(cve["exploitabilityScore3"]))
                elif "exploitabilityScore2" in cve:
                    if cve["exploitabilityScore2"] is not None and cve["exploitabilityScore2"] > 0:
                        w.append(float(cve["exploitabilityScore2"]))
                    else:
                        w.append(0.0)
            elif "exploitabilityScore2" in cve:
                if cve["exploitabilityScore2"] is not None and cve["exploitabilityScore2"] > 0:
                    w.append(float(cve["exploitabilityScore2"]))
            else:
                w.append(0.0)

        w = self.__scale_weights(w)            
        self.weights_likelihood = w
        self.criteria_likelihood = c
        labels = []
        for l in c: 
            labels.append(l + " W" + str(round(w[c.index(l)],2))) 

        df = pd.DataFrame(np.zeros((len(threat_chaining_list), len(self.criteria_likelihood))), columns=labels) 

        for a in range(0, len(threat_chaining_list)):
            if type(threat_chaining_list) == str and threat_chaining_list in self.criteria_likelihood: # if just a CAPEC/CVE by string
                df.iloc[a, self.criteria_likelihood.index(threat_chaining_list)] = 1
            elif type(threat_chaining_list) == list:
                for threat_id in threat_chaining_list[a]:
                    if type(threat_id) == list:
                        for id_ in threat_id:
                            if id_ in self.criteria_likelihood:
                                df.iloc[a, self.criteria_likelihood.index(id_)] = 1                                                                           
                    elif threat_id in self.criteria_likelihood: 
                        df.iloc[a, self.criteria_likelihood.index(threat_id)] = 1    

        self.dataframe_likelihood = df

    def __impact_maker(self, threat_chaining_list):
        """
        Build performance matrix for decision
        """
        
        self.__filter_distinct_threats(threat_chaining_list)
        w = [] # list of weights based on cvss3_score of cve
        c = [] # criteria CVE_id/CAPEC_id

        for cve in self.cves:
            c.append(cve["CVE"])
            if "impactScore3" in cve: 
                if cve["impactScore3"] is not None and cve["impactScore3"] > 0:
                    w.append(float(cve["impactScore3"]))
                elif "impactScore2" in cve:
                    if cve["impactScore2"] is not None and cve["impactScore2"] > 0:
                        w.append(float(cve["impactScore2"]))
                    else:
                        w.append(0.0)
            elif "impactScore2" in cve:
                if cve["impactScore2"] is not None and cve["impactScore2"] > 0:
                    w.append(float(cve["impactScore2"]))
            else:
                w.append(0.0)

        w = self.__scale_weights(w)            
        self.weights_impact = w
        self.criteria_impact = c
        labels = []
        for l in c: 
            labels.append(l + " W" + str(round(w[c.index(l)],2))) 

        df = pd.DataFrame(np.zeros((len(threat_chaining_list), len(self.criteria_impact))), columns=labels) 

        for a in range(0, len(threat_chaining_list)):
            if type(threat_chaining_list) == str and threat_chaining_list in self.criteria_impact: # if just a CAPEC/CVE by string
                df.iloc[a, self.criteria_impact.index(threat_chaining_list)] = 1
            elif type(threat_chaining_list) == list:
                for threat_id in threat_chaining_list[a]:
                    if type(threat_id) == list:
                        for id_ in threat_id:
                            if id_ in self.criteria_impact:
                                df.iloc[a, self.criteria_impact.index(id_)] = 1                                                                           
                    elif threat_id in self.criteria_impact: 
                        df.iloc[a, self.criteria_impact.index(threat_id)] = 1    

        self.dataframe_impact = df

    def __scale_weights(self, w):
        w_scaled = []
        sum_w = sum(w)
        for i in w:
            if sum_w == 0:
                w_scaled.append(0)
            else:
                w_scaled.append(i/sum_w)

        return w_scaled
  