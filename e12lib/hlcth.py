#########################################################
# E12Lib - hlcth                                        #
# Author: Antonio Horta <ajhorta@cybercrafter.com.br>   #
# https://gitlab.com/cybercrafter/e12lib                #
# Cybercrafter ® 2021                                   #
#########################################################

from .constant import *
from .kdd import KDDSec
from .killchain import Chain, Polymer, Link
from .priority import Priority

import numpy as np
import pandas as pd
import copy

# Machine Learning
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.cluster import AgglomerativeClustering

# Cross-validation
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import KFold

# PCA
from sklearn.decomposition import PCA

# New MCDA Lib
from scikitmcda.mcda import MCDA
from scikitmcda.topsis import TOPSIS
from scikitmcda.wsm import WSM
from scikitmcda.wpm import WPM
from scikitmcda.waspas import WASPAS
from scikitmcda.promethee_ii import PROMETHEE_II
from scikitmcda.electre_ii import ELECTRE_II
from scikitmcda.vikor import VIKOR
from scikitmcda.constants import MAX, MIN, LinearMinMax_, LinearMax_, LinearSum_, Vector_, EnhancedAccuracy_, Logarithmic_ 
from scikitmcda.ensemble import ENSEMBLE 

class Screener:
    def __init__(self, polymer):  # constructor method
        """
        Mount Screener to proceed high level cyber threat hunting

        :param polymer: Polymer instance 
        :type polymer: Polymer
        """
        try:
            self.polymer = polymer
        except Exception as e:
            print("Incompatible arguments. List of Chains and Polymer are required.")
            raise

    def training(self, clustering=False, algorithm=None):
        """
        Method to train the Polymer for HLCTH.       

        Choose among algorithms:

        DecisionTreeClassifier, MLPClassifier, RandomForestClassifier, 
        GaussianProcessClassifier, SVC, MultinomialNB, KNeighborsClassifier,
        GaussianNB, AdaBoostClassifier, QuadraticDiscriminantAnalysis,
        GradientBoostingClassifier and VotingClassifier

        :param clustering: Define strategy with cluster (True) or without cluster HAC (Default: False)
        :clustering type: boolean

        :param algorithm: Define Machine learning Algorithm ().   If Default:None, the best alg. will be choosed (slow)
        :algorithm type: string

        :return: {  'model': model['model'],
                    'algorithm': model['algorithm'],
                    'clustering': clustering,
                    'scores_mean': model['scores_mean'],
                    'prob_mean': model['prob_mean'],
                    'scatter': scatter
                     }

        :type return: dict
        """

        # Define X and Y
        X = self.polymer.binary_table.iloc[:, 2:].values
        Y = self.polymer.binary_table.iloc[:, 1].values
        
        # Var reduction using PCA: just for view
        pca = PCA(n_components=2)
        scatter = pca.fit_transform(X)
        # scatter = 0

        labels = Y
        if clustering is True:
            # Applying HAC - 2 clusters due to dendogram
            clusters = AgglomerativeClustering(n_clusters=2,
                                               affinity='euclidean',
                                               linkage='ward')
            clusters.fit(X)
            labels = clusters.labels_

        if algorithm is None:
            model = self.__get_best_model(X, labels)
        else:
            model = self.__get_model(algorithm, X, labels)

        return {'model': model['model'],
                'algorithm': model['algorithm'],
                'clustering': clustering,
                'scores_mean': model['scores_mean'],
                'prob_mean': model['prob_mean'],
                'scatter': scatter
                }

    def rank(self, threat_series, model_fit, mcda_alg="TOPSIS", wc1=0, wc2=0, wc3=0, wc4=0, wc5=0):
        """
        The method ranks considering 5 criteria. 
        
        :param threat_series: list of Chain object (Polymer)
        :type threat_series: list
        
        :param model_fit: Model trained for use in high-level Cyber Threat Hunting Method
        :type model_fit: Model (SciKit-Learning)
        
        :param mcda_alg: Name of the MCDA methos used for HLCTH. Default is "TOPSIS". Use: WSM, WPM, WASPAS, PROMETHEEII, VIKOR, ELECTREII
        :type mcda_alg: string

        :param wc1: Weight of criterion Risk.  Default is 0 to use entropy 
        :type wc1: float value range from 0 to 1
        
        :param wc2: Weight of criterion Chain Size.  Default is 0 to use entropy 
        :type wc2: float value range from 0 to 1
        
        :param wc3: Weight of criterion Impact.  Default is 0 to use entropy 
        :type wc3: float value range from 0 to 1
        
        :param wc4: Weight of criterion Probability.  Default is 0 to use entropy 
        :type wc4: float value range from 0 to 1

        :param wc5: Weight of criterion Complexity.  Default is 0 to use entropy 
        :type wc5: float value range from 0 to 1
        """        
        list_chain_id = []
        c1 = []
        c2 = []
        c3 = []
        c3 = []
        c4 = []
        c5 = []

        for s in threat_series:            
            list_chain_id.append({"chain_id": s.chain_id})
            pred = self.__criteria_predict(s, model_fit)
            c1.append(pred[0])
            c2.append(s.c2)
            c3.append(s.c3)
            c4.append(pred[1])
            c5.append(s.c5)
        
        # Convert c1 to num
        c1n = []
        for c in c1:
            if c == 'intrusion-set':
                c1n.append(2)
            elif c == 'malware':
                c1n.append(1)
            elif c == 'tool':
                c1n.append(0)
            elif c == 0:
                c1n.append(1)
            elif c == 1:
                c1n.append(0)

        # alternatives/hypotheses
        a = np.vstack((c1n, c2, c3, c4, c5)).T

        sign = [1, 1, 1, 1, 1] # All MAX

        # Weights
        # w = [0.15, 0.15, 0.40, 0.10, 0.20]
        entropy = False
        if 0 in [wc1, wc2, wc3, wc4, wc5]: 
            w = self.__mcda_entropy(a, sign)
            entropy = True
        else:
            w = [wc1, wc2, wc3, wc4, wc5]
       
        return {"ranking": self.__mcda_ranking(a, w, sign, mcda_alg, list_chain_id), "weights": w, "criteria": a, "entropy": entropy, "mcda": mcda_alg, "ml": model_fit["algorithm"], "clustering":model_fit["clustering"]}

    def rank_with_severity(self, threat_series, threat_series_severity, model_fit, mcda_alg="TOPSIS", wc1=0, wc2=0, wc3=0, wc4=0, wc5=0, wc6=0, ensemble_severity=False):
        """
        The method ranks considering C6 criterion (Severity). 
        
        :param threat_series: list of Chain object (Polymer)
        :type threat_series: list
        
        :param threat_series_severity: list of CAPEC ou CVE found in each Chain
        :type threat_series_severity: list of list
        
        :param model_fit: Model trained for use in high-level Cyber Threat Hunting Method
        :type model_fit: Model (SciKit-Learning)
        
        :param mcda_alg: Name of the MCDA methos used for HLCTH. Default is "TOPSIS". Use: WSM, WPM, WASPAS, PROMETHEEII, VIKOR, ELECTREII
        :type mcda_alg: string

        :param wc1: Weight of criterion Risk.  Default is 0 to use entropy 
        :type wc1: float value range from 0 to 1
        
        :param wc2: Weight of criterion Chain Size.  Default is 0 to use entropy 
        :type wc2: float value range from 0 to 1
        
        :param wc3: Weight of criterion Impact.  Default is 0 to use entropy 
        :type wc3: float value range from 0 to 1
        
        :param wc4: Weight of criterion Probability.  Default is 0 to use entropy 
        :type wc4: float value range from 0 to 1

        :param wc5: Weight of criterion Complexity.  Default is 0 to use entropy 
        :type wc5: float value range from 0 to 1

        :param wc6: Weight of criterion Severity.  Default is 0 to use entropy 
        :type wc6: float value range from 0 to 1

        :param ensemble_severity: If False (default), if uses the same method defined in mcda_alg for ranking severity, 
                                else if (True) the severity will be calculated by ensemble average using TOPSIS, WASPAS and PROMETHEE II 
        :type ensemble_severity: boolean
        """

        list_chain_id = []
        c1 = []
        c2 = []
        c3 = []
        c3 = []
        c4 = []
        c5 = []
        c6 = []

        if ensemble_severity is False: 
            severity_score = Priority()
            severity_score.ranking_by_chain_severity(threat_series_severity, mcda=mcda_alg)
        else:
            severity_score_waspas = Priority()
            severity_score_waspas.ranking_by_chain_severity(threat_series_severity, mcda="WASPAS")
            severity_score_topsis = Priority()
            severity_score_topsis.ranking_by_chain_severity(threat_series_severity, mcda="TOPSIS")
            severity_score_promethee_ii = Priority()
            severity_score_promethee_ii.ranking_by_chain_severity(threat_series_severity, mcda="PROMETHEE_II")
            severity_score = ENSEMBLE()
            severity_score.ranking_by_voting([severity_score_waspas.method, severity_score_topsis.method, severity_score_promethee_ii.method])

        for s in threat_series:            
            list_chain_id.append({"chain_id": s.chain_id}),
            pred = self.__criteria_predict(s, model_fit)
            c1.append(pred[0])
            c2.append(s.c2)
            c3.append(s.c3)
            c4.append(pred[1])
            c5.append(s.c5)
            c6.append(severity_score.df_decision.iloc[threat_series.index(s), -2]) # Severity chain score

        # Convert c1 to num
        c1n = []
        for c in c1:
            if c == 'intrusion-set':
                c1n.append(2)
            elif c == 'malware':
                c1n.append(1)
            elif c == 'tool':
                c1n.append(0)
            elif c == 0:
                c1n.append(1)
            elif c == 1:
                c1n.append(0)

        # alternatives/hypotheses
        a = np.vstack((c1n, c2, c3, c4, c5, c6)).T

        sign = [1, 1, 1, 1, 1, 1] # All MAX

        # Weights
        # w = [0.15, 0.15, 0.40, 0.10, 0.20]
        entropy = False
        if 0 in [wc1, wc2, wc3, wc4, wc5, wc6]: 
            w = self.__mcda_entropy(a, sign)
            entropy = True
        else:
            w = [wc1, wc2, wc3, wc4, wc5, wc6]
        
        sign = [1, 1, 1, 1, 1, 1] # All MAX
        
        return {"ranking": self.__mcda_ranking(a, w, sign, mcda_alg, list_chain_id), "weights": w, "criteria": a, "entropy": entropy, "mcda": mcda_alg, "ml": model_fit["algorithm"], "clustering":model_fit["clustering"]}

    def __criteria_predict(self, serie, model_fit):

        nu_serie = copy.deepcopy(serie)
        c1 = []
        c4 = []
        del nu_serie.binary_table_row[0]
        del nu_serie.binary_table_row[0]                
        c1 = model_fit["model"].predict(np.array(nu_serie.binary_table_row).reshape(1, -1))
        c4 = model_fit["model"].predict_proba(np.array(nu_serie.binary_table_row).reshape(1, -1)).max()
        return c1, c4

    def __get_best_model(self, X, labels):
        best_model = {"scores_mean": 0}
        for algorithm in CLFS:
            clf = self.__get_model(algorithm, X, labels)
            if clf["scores_mean"] > best_model["scores_mean"]:
                best_model = clf

        return best_model

    def __get_model(self, algorithm, X, labels):

        if algorithm == "DecisionTreeClassifier":
            model = Pipeline([
                ('clf', DecisionTreeClassifier(random_state=0, max_depth=10))
                ])
        if algorithm == "MLPClassifier":
            model = Pipeline([
                ('clf', MLPClassifier(random_state=0,
                                        alpha=1,
                                        max_iter=1000))
                ])
        if algorithm == "RandomForestClassifier":
            model = Pipeline([
                ('clf', RandomForestClassifier(n_estimators=45,
                                                random_state=0))
                ])
        if algorithm == "GaussianProcessClassifier":
            model = Pipeline([
                ('clf', GaussianProcessClassifier(1.0 * RBF(1.0)))
                ])
        if algorithm == "SVC":
            model = Pipeline([
                ('clf', SVC(random_state=0,
                            kernel="linear",
                            probability=True))
                ])
        if algorithm == "MultinomialNB":
            model = Pipeline([
                ('clf', MultinomialNB())
                ])
        if algorithm == "KNeighborsClassifier":
            model = Pipeline([
                ('clf', KNeighborsClassifier(3))
                ])
        if algorithm == "GaussianNB":
            model = Pipeline([
                ('clf', GaussianNB())
                ])
        if algorithm == "AdaBoostClassifier":
            model = Pipeline([
                ('clf', AdaBoostClassifier(random_state=0))
                ])
        if algorithm == "QuadraticDiscriminantAnalysis":
            model = Pipeline([
                ('clf', QuadraticDiscriminantAnalysis())
                ])
        if algorithm == "GradientBoostingClassifier":
            model = Pipeline([
                ('clf', GradientBoostingClassifier(random_state=0))
                ])
        if algorithm == "VotingClassifier":
            clf1 = DecisionTreeClassifier(random_state=0,
                                          max_depth=10)
            clf2 = MLPClassifier(random_state=0, alpha=1, max_iter=1000)
            clf3 = RandomForestClassifier(random_state=0)  # was 45
            clf4 = GaussianProcessClassifier(1.0 * RBF(1.0))
            clf5 = SVC(random_state=0, kernel="linear", probability=True)
            clf6 = MultinomialNB()
            clf7 = KNeighborsClassifier(3)
            clf8 = GaussianNB()
            clf9 = AdaBoostClassifier(random_state=0)
            clf10 = QuadraticDiscriminantAnalysis()
            clf11 = GradientBoostingClassifier(random_state=0)
            model = Pipeline([
                ('eclf', VotingClassifier(estimators=[
                                                        # ('dtc', clf1),
                                                        ('mlpc', clf2),
                                                        # ('rfc', clf3),
                                                        ('gpc', clf4),
                                                        ('svc', clf5),
                                                        # ('mnb', clf6),
                                                        # ('knc', clf7),
                                                        # ('gnb', clf8),
                                                        # ('abc', clf9),
                                                        # ('qda', clf10),
                                                        ('gbc', clf11)
                                                    ], voting='soft'))
                                                ])

        # cross-validation for global values
        scores = cross_val_score(model, X, labels, cv=KFold(n_splits=15))
        prob = cross_val_predict(model, X, labels, cv=KFold(n_splits=15), method='predict_proba').max(axis=1)

        model.fit(X, labels)

        return {'model': model,
                'algorithm': algorithm,
                'scores_mean': scores.mean(),
                'prob_mean': prob.mean(axis=0)
                }

    def __mcda_ranking(self, a, w, sign, mcda_alg, mcda_list):

        ranking = []

        if mcda_alg == "TOPSIS":

            topsis = TOPSIS()            
            topsis.dataframe(a)
            topsis.set_weights_manually(w)
            topsis.set_signals(sign)            
            topsis.decide()
            ranking = topsis.df_decision.iloc[:, -2]

        if mcda_alg == "WSM":

            wsm = WSM()            
            wsm.dataframe(a)
            wsm.set_weights_manually(w)
            wsm.set_signals(sign)            
            wsm.decide()
            ranking = wsm.df_decision.iloc[:, -2]

        if mcda_alg == "WPM":

            wpm = WPM()            
            wpm.dataframe(a)
            wpm.set_weights_manually(w)
            wpm.set_signals(sign)            
            wpm.decide()
            ranking = wpm.df_decision.iloc[:, -2]

        if mcda_alg == "WASPAS":

            waspas = WASPAS()            
            waspas.dataframe(a)
            waspas.set_weights_manually(w)
            waspas.set_signals(sign)            
            waspas.decide()
            ranking = waspas.df_decision.iloc[:, -2]

        if mcda_alg == "PROMETHEEII":

            promethee_ii = PROMETHEE_II()            
            promethee_ii.dataframe(a)
            promethee_ii.set_weights_manually(w)
            promethee_ii.set_signals(sign)            
            promethee_ii.decide()
            ranking = promethee_ii.df_decision.iloc[:, -2]

        if mcda_alg == "ELECTREII":

            electre_ii = ELECTRE_II()            
            electre_ii.dataframe(a)
            electre_ii.set_weights_manually(w)
            electre_ii.set_signals(sign)            
            electre_ii.decide()
            ranking = electre_ii.df_decision.iloc[:, -2]

        if mcda_alg == "VIKOR":

            vikor = VIKOR()            
            vikor.dataframe(a)
            vikor.set_weights_manually(w)
            vikor.set_signals(sign)            
            vikor.decide()
            # invert rank
            ranking = 1 - vikor.df_decision.iloc[:, -2]

        for we in list(ranking):
            mcda_list[list(ranking).index(we)]["weigth"] = "{:.2f}".format(we)

        priority_list = np.argsort(ranking)[::-1][:]

        mcda_list = [mcda_list[i] for i in priority_list]

        return mcda_list

    def __mcda_entropy(self, a, signs):
        mcda = MCDA()
        mcda.dataframe(a)
        mcda.set_signals(signs)
        mcda.set_weights_by_entropy()
        return mcda.weights

