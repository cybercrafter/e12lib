from e12lib.kdd import KDDSec, Knowledge

kddsec = KDDSec()
knowledge = kddsec.load(version=9.0)

print("ATT&CK Version: ", knowledge.attack_version)
print("Tactics: ", len(knowledge.tactics))
print("Techniques: ", len(knowledge.techniques))
print("Mitigations: ", len(knowledge.mitigations))
print("Groups: ", len(knowledge.groups))
print("Malwares: ", len(knowledge.malwares))
print("Tools: ", len(knowledge.tools))
print("Procedures: ", len(knowledge.procedures))
print("Relationships: ", len(knowledge.relationships))
for i in knowledge.mitigations:
    print(i['mitre_attack_element_id'], i['name'], i['external_id'])

ATT&CK Version:  ATT&CK-v8.2
Tactics:  18
Techniques:  1002
Mitigations:  294
Groups:  114
Malwares:  463
Tools:  66
Procedures:  643
Relationships:  10718

ATT&CK Version:  ATT&CK-v9.0
Tactics:  18
Techniques:  1113
Mitigations:  295
Groups:  127
Malwares:  524
Tools:  72
Procedures:  723
Relationships:  12549