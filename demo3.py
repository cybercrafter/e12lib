from scikitmcda.ensemble import ENSEMBLE 
from tabulate import tabulate
from e12lib.priority import Priority

# Test of severity
threats_topsis = Priority()
threats_waspas = Priority()
threats_promethee_ii = Priority()
threats_electre_ii = Priority()

threat_series_severity = [["CAPEC-564", "CAPEC-49",
                           "CAPEC-127", "CAPEC-497", 
                           "CAPEC-93", "CAPEC-569", 
                           "CAPEC-177", "CAPEC-203", 
                           "CAPEC-300", "CAPEC-643", 
                           "CAPEC-267", "CAPEC-98", 
                           "CAPEC-640", "CAPEC-555", 
                           "CAPEC-552", "CAPEC-557",
                           "CAPEC-437", "CAPEC-438",
                           "CAPEC-439", "CAPEC-309",
                           "CAPEC-577", "CAPEC-560",
                           "CVE-2019-19781", "CVE-2020-10189", 
                           "CVE-2019-19781", "CVE-2020-10189", 
                           "CVE-2019-19781",   
                           "CVE-2015-1641", "CVE-2017-0199",  
                           "CVE-2019-3396"], # 1. APT41
                          ["CAPEC-564", "CAPEC-267",
                           "CAPEC-576", "CAPEC-98",
                           "CAPEC-573", "CAPEC-580", 
                           "CAPEC-312", 
                           "CVE-2012-0158", "CVE-2014-1761", 
                           "CVE-2018-0802"], # 2. Inception
                          ["CAPEC-267", "CAPEC-98",
                           "CAPEC-573", "CAPEC-557",
                           "CAPEC-312", "CAPEC-309",
                           "CAPEC-577", "CVE-2017-11882"],     # 3. Frankenstein
                          ["CAPEC-575", "CAPEC-564",
                           "CAPEC-555", "CAPEC-93",
                           "CAPEC-94", "CAPEC-177",
                           "CAPEC-203", "CAPEC-643",
                           "CAPEC-267", "CAPEC-576",
                           "CAPEC-98", "CAPEC-640",
                           "CAPEC-652", "CAPEC-309",
                           "CAPEC-577", "CAPEC-560",
                           "CVE-2020-1472", "CVE-2017-0144"],  # 4. Wizard Spider
                          ["CAPEC-564", "CAPEC-564",
                           "CAPEC-93", "CAPEC-300",
                           "CAPEC-267", "CAPEC-98",
                           "CAPEC-640", "CAPEC-555",
                           "CAPEC-557", "CAPEC-580",
                           "CVE-2018-8174", "CVE-2017-8570", 
                           "CVE-2017-0199", "CVE-2017-8759"]]  # 5. Cobalt Group 

alternatives = ["APT41", "Inception", "Frankenstein", "Wizard Spider", "Cobalt Group"]

# Ranking with TOPSIS
threats_topsis.ranking_by_chain_severity(threat_series_severity, alt_labels=alternatives)
print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))
print(threats_topsis.dataframe)

# Ranking WASPAS
threats_waspas.ranking_by_chain_severity(threat_series_severity, "WASPAS", alternatives)

# Ranking ELECTRE_II
threats_electre_ii.ranking_by_chain_severity(threat_series_severity, "ELECTRE_II", alternatives)

# Ranking PROMETHEE_II
threats_promethee_ii.ranking_by_chain_severity(threat_series_severity, "PROMETHEE_II", alternatives)


print(threats_topsis.weights)


# Ranking ENSEMBLE
threats_voting_soft = ENSEMBLE()
threats_voting_soft.ranking_by_voting([threats_waspas.method, threats_topsis.method, threats_electre_ii.method, threats_promethee_ii.method], voting="hard")
print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))
