from scikitmcda.ensemble import ENSEMBLE 
from tabulate import tabulate
from e12lib.priority import Priority

# Test of severity
threats_topsis = Priority()
threats_waspas = Priority()
threats_electre_ii = Priority()

threat_series_severity = [["CVE-2020-6316",  "CVE-2020-6268",
                           "CVE-2020-26807", "CVE-2019-0325",
                           "CVE-2020-8983", "CVE-2019-18225"], # 1. Sys1
                          ["CVE-2020-8983", "CVE-2019-18225"], # 2. Sys2
                          ["CVE-2021-2113"],                   # 3. Sys3
                          ["CVE-2019-6183",  "CVE-2017-0144"], # 4. Sys4
                          ["CVE-2020-4383",  "CVE-2020-4382"], # 5. Sys5
                          ["CVE-2020-6316",  "CVE-2020-6268",
                           "CVE-2020-26807", 
                           "CVE-2020-8983", "CVE-2019-18225"], # 6. Sys6
                          ["CVE-2020-8983"],                   # 7. Sys7
                          ["CVE-2021-2113"],                   # 8. Sys8
                          ["CVE-2017-0144"],                   # 9. Sys9
                          ["CVE-2020-4383",  "CVE-2020-4382",
                           "CVE-2020-6316",  "CVE-2020-6268",                           
                           "CVE-2020-8983", "CVE-2019-18225"], # 10. Sys10
                          ["CVE-2020-6316",  "CVE-2020-6268"], # 11. Sys11                         
                          ["CVE-2020-8983", "CVE-2019-18225"], # 12. Sys12
                          ["CVE-2020-8983", "CVE-2019-18225"], # 13. Sys13
                          ["CVE-2021-2113"],                   # 14. Sys14
                          ["CVE-2019-6183",  "CVE-2017-0144"], # 15. Sys15
                          ["CVE-2020-4383",  "CVE-2020-4382",
                           "CVE-2020-8983", "CVE-2019-18225"]]  #16. Sys16

alternatives = ["Sys1", "Sys2", "Sys3", "Sys4", "Sys5", "Sys6", "Sys7", "Sys8", "Sys9", "Sys10", "Sys11", "Sys12", "Sys13", "Sys14", "Sys15", "Sys16" ]

# Setting Impact by Rank Weight 

# Ranking TOPSIS
threats_topsis.ranking_by_chain_risk(threat_series_severity, alt_labels=alternatives, impact_ranking=4)
print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))

# Ranking with WASPAS
threats_waspas.ranking_by_chain_risk(threat_series_severity, mcda="WASPAS", alt_labels=alternatives, impact_ranking=4)
print(tabulate(threats_waspas.df_decision, threats_waspas.df_decision.columns, tablefmt="psql"))

# Ranking with ELECTRE II
threats_electre_ii.ranking_by_chain_risk(threat_series_severity, mcda="ELECTRE_II", alt_labels=alternatives, impact_ranking=4)
print(tabulate(threats_electre_ii.df_decision, threats_electre_ii.df_decision.columns, tablefmt="psql"))

# Ranking ENSEMBLE
threats_voting_soft = ENSEMBLE()
threats_voting_soft.ranking_by_voting([threats_topsis.method, threats_waspas.method, threats_electre_ii.method])
print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))
