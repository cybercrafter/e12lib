POLYMER_VERSION = 9

TACTICS_V8 = [{"position": 1, "polymer_phase": "Reconnaissance", "attack_tactic": "reconnaissance", "polymer_category": "Preparation"},
              {"position": 2, "polymer_phase": "Resource Development", "attack_tactic": "resource-development", "polymer_category": "Preparation"},
              {"position": 3, "polymer_phase": "Initial Access", "attack_tactic": "initial-access", "polymer_category": "Access"},
              {"position": 4, "polymer_phase": "Execution", "attack_tactic": "execution", "polymer_category": "Foothold"},
              {"position": 5, "polymer_phase": "Persistence", "attack_tactic": "persistence", "polymer_category": "Foothold"},
              {"position": 6, "polymer_phase": "Privilege Escalation", "attack_tactic": "privilege-escalation", "polymer_category": "Foothold"},
              {"position": 7, "polymer_phase": "Defense Evasion", "attack_tactic": "defense-evasion", "polymer_category": "Evasion"},
              {"position": 8, "polymer_phase": "Credential Access", "attack_tactic": "credential-access", "polymer_category": "Provisioning"},
              {"position": 9, "polymer_phase": "Discovery", "attack_tactic": "discovery","polymer_category": "Provisioning"},
              {"position": 10, "polymer_phase": "Command and Control", "attack_tactic": "command-and-control", "polymer_category": "Propagation"},
              {"position": 11, "polymer_phase": "Lateral Movement", "attack_tactic": "lateral-movement", "polymer_category": "Propagation"},
              {"position": 12, "polymer_phase": "Remote Service Effects", "attack_tactic": "remote-service-effects", "polymer_category": "Objectives"},
              {"position": 13, "polymer_phase": "Network Effects", "attack_tactic": "network-effects", "polymer_category": "Objectives"},
              {"position": 14, "polymer_phase": "Collection", "attack_tactic": "collection", "polymer_category": "Objectives"},
              {"position": 15, "polymer_phase": "Exfiltration", "attack_tactic": "exfiltration", "polymer_category": "Objectives"},
              {"position": 16, "polymer_phase": "Inhibit Response Function", "attack_tactic": "inhibit-response-function", "polymer_category": "Objectives"},
              {"position": 17, "polymer_phase": "Impair Process Control", "attack_tactic": "impair-process-control", "polymer_category": "Objectives"},
              {"position": 18, "polymer_phase": "Impact", "attack_tactic": "impact", "polymer_category": "Objectives"}
              ]

TACTICS_V9 = [{"position": 1, "polymer_phase": "Reconnaissance", "attack_tactic": "reconnaissance", "polymer_category": "Preparation"},
              {"position": 2, "polymer_phase": "Resource Development", "attack_tactic": "resource-development", "polymer_category": "Preparation"},
              {"position": 3, "polymer_phase": "Initial Access", "attack_tactic": "initial-access", "polymer_category": "Access"},
              {"position": 4, "polymer_phase": "Execution", "attack_tactic": "execution", "polymer_category": "Foothold"},
              {"position": 5, "polymer_phase": "Persistence", "attack_tactic": "persistence", "polymer_category": "Foothold"},
              {"position": 6, "polymer_phase": "Privilege Escalation", "attack_tactic": "privilege-escalation", "polymer_category": "Foothold"},
              {"position": 7, "polymer_phase": "Defense Evasion", "attack_tactic": "defense-evasion", "polymer_category": "Evasion"},
              {"position": 8, "polymer_phase": "Credential Access", "attack_tactic": "credential-access", "polymer_category": "Provisioning"},
              {"position": 9, "polymer_phase": "Discovery", "attack_tactic": "discovery","polymer_category": "Provisioning"},
              {"position": 10, "polymer_phase": "Command and Control", "attack_tactic": "command-and-control", "polymer_category": "Propagation"},
              {"position": 11, "polymer_phase": "Lateral Movement", "attack_tactic": "lateral-movement", "polymer_category": "Propagation"},
              {"position": 12, "polymer_phase": "Remote Service Effects", "attack_tactic": "remote-service-effects", "polymer_category": "Objectives"},
              {"position": 13, "polymer_phase": "Network Effects", "attack_tactic": "network-effects", "polymer_category": "Objectives"},
              {"position": 14, "polymer_phase": "Collection", "attack_tactic": "collection", "polymer_category": "Objectives"},
              {"position": 15, "polymer_phase": "Exfiltration", "attack_tactic": "exfiltration", "polymer_category": "Objectives"},
              {"position": 16, "polymer_phase": "Inhibit Response Function", "attack_tactic": "inhibit-response-function", "polymer_category": "Objectives"},
              {"position": 17, "polymer_phase": "Impair Process Control", "attack_tactic": "impair-process-control", "polymer_category": "Objectives"},
              {"position": 18, "polymer_phase": "Impact", "attack_tactic": "impact", "polymer_category": "Objectives"}
              ]


REPO_FOLDER = "download"

MITRE_CTI_V8 = ["enterprise-attack",
                "mobile-attack",
                "ics-attack"
                ]

MITRE_CTI_V9 = ["enterprise-attack",
                "mobile-attack",
                "ics-attack"
                ]

CLFS = ["DecisionTreeClassifier",
        "MLPClassifier",
        "RandomForestClassifier",
        "GaussianProcessClassifier",
        "SVC",
        "MultinomialNB",
        "KNeighborsClassifier",
        "GaussianNB",
        "AdaBoostClassifier",
        "QuadraticDiscriminantAnalysis",
        "GradientBoostingClassifier"
        ]
