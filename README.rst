======
E12Lib
======

Python Library to perform high level cyber threat hunting using polymer kill chain.

Polymer version 9 supports att&ck versions 8 and 9

E12Lib is the basis of Episode 12
---------------------------------

.. image:: https://img.youtube.com/vi/ScEw5O_j7xA/0.jpg
   :alt: Episode 12 demonstration video
   :align: left
   :target: https://www.youtube.com/watch?v=ScEw5O_j7xA
   :class: with-shadow float-left
   

Episode 12 is a tool made for POC and whatever you need for threat hunting. E12 identifies intrusion patterns and presents statistical data on complex attack patterns and gap analysis to improve the accuracy of the threat hunting process. The E12 is a result of a deep study of attack patterns based on Mitre ATT&CK becoming the first platform performing the concept of High-level Cyber Threat Hunting with Polymer Kill Chain for threat intelligence processes driven by multi-criteria and artificial inteligence.

The basis for the concepts demonstrated in Episode 12 comes from two articles published and presented by the author at the IEEE International Workshop on Big Data Analytics for Cyber Threat Hunting (CyberHunt 2020) and the 6th International Conference on Internet of Things, Big Data and Security 2021.

E12Lib is the main gear for performing high-level cyber threat hunting using the polymer kill chain. Its Apache license allows the use of this library in any project related to the hunting of cyber threats that need to prioritize suspicions of threats at a high level or demonstrate them using the polymer kill chain.


Installing
----------

1. Install from private repo

.. code-block:: python

    # install from private
    python3 setup.py sdist bdist_wheel
    pip install git+ssh://git@gitlab.com/cybercrafter/e12lib.git


Quick start
-----------

1. KDDSec process

.. code-block:: python

    from E12Lib.kdd import KDDSec, Knowledge

    kddsec = KDDSec()
    knowledge = kddsec.load()

    print("ATT&CK Version: ", knowledge.attack_version)
    print("Tactics: ", len(knowledge.tactics))
    print("Techniques: ", len(knowledge.techniques))
    print("Mitigations: ", len(knowledge.mitigations))
    print("Groups: ", len(knowledge.groups))
    print("Malwares: ", len(knowledge.malwares))
    print("Tools: ", len(knowledge.tools))
    print("Procedures: ", len(knowledge.procedures))
    print("Relationships: ", len(knowledge.relationships))
    print("Binary Table:\n", knowledge.binary_table)


    ATT&CK Version:  ATT&CK-v8.1
    Tactics:  18
    Techniques:  786
    Mitigations:  294
    Groups:  113
    Malwares:  459
    Tools:  64
    Procedures:  654
    Relationships:  10567
    Binary Table:

             procedure_id           type  T0843  T0863  T0855  T0852  T0871  ...  T1564  T1552.002  
    0    intrusion-set...  intrusion-set    0.0    0.0    0.0    1.0    0.0  ...    0.0        0.0  
    1    intrusion-set...  intrusion-set    0.0    0.0    0.0    1.0    0.0  ...    0.0        0.0  
    ..                ...            ...    ...    ...    ...    ...    ...  ...    ...        ...  
    652  tool--c865526...           tool    0.0    0.0    0.0    0.0    0.0  ...    0.0        0.0  
    653  tool--afc079f...           tool    0.0    0.0    0.0    0.0    0.0  ...    0.0        0.0  

    [654 rows x 788 columns]

2. Criteria extraction by Polymer Kill Chain

.. code-block:: python

    from E12Lib.kdd import KDDSec, Knowledge
    from E12Lib.killchain import Polymer, Link, Chain

    kddsec = KDDSec()
    knowledge = kddsec.load()
    polymer = Polymer(knowledge)

    print("Polymer Version: ", polymer.version)
    print("ATT&CK Version: ", polymer.attack_version)
    print("Platforms: ", len(polymer.platforms))
    print("Techniques: ", len(polymer.techniques))
    print("Polymer phases: ", len(polymer.phases))
    print("Binary Table (shape): ", polymer.binary_table.shape)

    Polymer Version:  8
    ATT&CK Version:  ATT&CK-v8.1
    Platforms:  35
    Techniques:  786
    Polymer phases:  18
    Binary Table (shape):  (654, 788)


    links = []
    links.append(Link(polymer, "T1548", "Windows"))
    links.append(Link(polymer, "T1197", "Windows"))
    links.append(Link(polymer, "T1134", "Windows"))

    for l in links:
        print(vars(l))

    {'external_id': 'T1548', 'impact': 7, 'platform': 'Windows', 'source_repo': 'ics-attack', 'date_time': None, 'log_id': None}
    {'external_id': 'T1197', 'impact': 5, 'platform': 'Windows', 'source_repo': 'ics-attack', 'date_time': None, 'log_id': None}
    {'external_id': 'T1134.003', 'impact': 6, 'platform': 'Windows', 'source_repo': 'ics-attack', 'date_time': None, 'log_id': None}

    chain = Chain(links)

    print(vars(chain))

    {'chain_id': 'attack-chaining--5d59d1aef177c621fdc441177dbee990', 
     'chain': [<__main__.Link object at 0x7f0345994208>, 
               <__main__.Link object at 0x7f0345994940>, 
               <__main__.Link object at 0x7f03459949e8>], 
     'c2': 3,
     'c3': 7,
     'c5': 3}

3. Triage of APTs (High Level Cyber Threat Hunting using Machine Learning anf Multi-Criteria Decision Aid Methods) 

.. code-block:: python
    
    from E12Lib.kdd import KDDSec, Knowledge
    from E12Lib.killchain import Polymer, Link, Chain
    from E12Lib.hlcth import Screener

    kddsec = KDDSec()
    knowledge = kddsec.load()
    polymer = Polymer(knowledge)

    screener = Screener(polymer)
    model_fit = screener.training(False, "GradientBoostingClassifier")

    print(model_fit)
    'model': Pipeline(steps=[('clf', GradientBoostingClassifier(random_state=0))]),
                      'algorithm': 'GradientBoostingClassifier',
                      'clustering': False,
                      'scores_mean': 0.7232188999630861,
                      'prob_mean': 0.7218150728921037,
                      'scatter': 0}

    # Strider
    threat = []
    threat.append(Link(polymer, "T1090.001", "Android"))
    threat.append(Link(polymer, "T1564.005", "Linux"))
    threat.append(Link(polymer, "T1556.002", "Windows"))
    threat_chaining_1 = Chain(threat)
    
    # PunchTrack
    threat = []
    threat.append(Link(polymer, "CAPEC-267", "AWS"))
    threat.append(Link(polymer, "T1027", "Data Historian"))
    threat.append(Link(polymer, "T1005", "Windows"))
    threat.append(Link(polymer, "T1074.001", "Windows"))
    threat_chaining_2 = Chain(threat)
    
    # ASPXSpy
    threat = []
    threat.append(Link(polymer, "CAPEC-650", "AWS"))
    threat.append(Link(polymer, "T1505.003", "Data Historian"))
    threat_chaining_3 = Chain(threat)
    
    threat_series = [threat_chaining_1, threat_chaining_2, threat_chaining_3]

    ranking = screener.rank(threat_series, model_fit)

    print("Strider is ", threat_chaining_1.chain_id)
    print("PunchTrack is ", threat_chaining_2.chain_id)
    print("ASPXSpy is ", threat_chaining_3.chain_id)

    Strider is  attack-chaining--8e59927afdc44f6604dfab33bb71caba
    PunchTrack is  attack-chaining--6c0cf077494a1733111210836f525594
    ASPXSpy is  attack-chaining--770b564eb95d6056c6d1a08927b3e89c
    
    print(ranking)

    {'ranking': [{'chain_id': 'attack-chaining--4f12a8d60e53917ec878218be3c55aa8', 'weigth': '0.74'},
                 {'chain_id': 'attack-chaining--837a6989a2f8bbd1be603a50fc302113', 'weigth': '0.67'},
                 {'chain_id': 'attack-chaining--5be6cf26f9825ed245dc34240df58ac8', 'weigth': '0.00'}],
     'weights': [1.2088752188328116e-15, 0.43216863343405043, 0.38073075811054913, 1.2088752188328116e-15, 0.18710060845539803],
     'entropy': True,
     'mcda': 'TOPSIS',
     'ml': 'GradientBoostingClassifier',
     'clustering': False}

4. Defining criterion severity to perform HLCTH with 6 criteria

.. code-block:: python

    from e12lib.kdd import KDDSec
    from e12lib.hlcth import Screener
    from e12lib.killchain import Polymer, Link, Chain
    import pprint

    threat_series_severity = [["CVE-2017-7269", "CVE-2014-6352"], # CVEs threat_chaining_1
                              ["CVE-2012-0158", "CVE-2017-0199"], # CVEs threat_chaining_2
                              ["CVE-2014-4114", "CVE-2018-0798"]] # CVEs threat_chaining_3

    ranking_with_severity = screener.rank_with_severity(threat_series, threat_series_severity, model_fit)
    print("\nRanking with Criteria C6 (Severity) \n")
    pprint.pprint(ranking_with_severity)

    +----+----------------+-----------------------+-----------------------+-------+---------------------+--------+
    |    | alternatives   |   CVE-2017-7269 W0.18 |   CVE-2014-6352 W0.17 |   ... |   performance score |   rank |
    |----+----------------+-----------------------+-----------------------+-------+---------------------+--------|
    |  0 | A1             |                     1 |                     1 |   ... |            0.433679 |      1 |
    |  1 | A2             |                     0 |                     0 |   ... |            0.394712 |      3 |
    |  2 | A3             |                     0 |                     0 |   ... |            0.413476 |      2 |
    +----+----------------+-----------------------+-----------------------+-------+---------------------+--------+

5. Test of severity ranking using ENSEMBLE (Voting)

.. code-block:: python
    
    from scikitmcda.ensemble import ENSEMBLE 
    from tabulate import tabulate
    from e12lib.priority import Priority

    # Test of severity
    threats_topsis = Priority()
    threats_waspas = Priority()
    threats_promethee_ii = Priority()

    threat_series_severity = [["CVE-2019-19781", "CVE-2020-10189", 
                               "CVE-2019-19781", "CVE-2020-10189", 
                               "CVE-2019-19781", "CVE-2012-0158",  
                               "CVE-2015-1641", "CVE-2017-0199",  
                               "CVE-2017-11882", "CVE-2019-3396"], # 1. APT41
                              ["CVE-2012-0158", "CVE-2014-1761", 
                               "CVE-2017-11882", "CVE-2018-0802"], # 2. Inception
                              ["CVE-2017-11882"],                  # 3. Frankenstein
                              ["CVE-2020-1472", "MS17-010"],       # 4. Wizard Spider
                              ["CVE-2018-8174", "CVE-2017-8570", 
                               "CVE-2017-0199", "CVE-2017-875"]]   # 5. Cobalt Group 


    alternatives = ["APT41", "Inception", "Frankenstein", "Wizard Spider", "Cobalt Group"]

    # Ranking with TOPSIS
    threats_topsis.ranking_by_chain_severity(threat_series_severity, alt_labels=alternatives)
    print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))

    +----+----------------+------------------------+------------------------+---------------------+---------------------+--------+
    |    | alternatives   |   CVE-2019-19781 W0.09 |   CVE-2020-10189 W0.09 |   ...               |   performance score |   rank |
    |----+----------------+------------------------+------------------------+---------------------+---------------------+--------|
    |  0 | APT41          |                      1 |                      1 |   ...               |            0.531377 |      1 |
    |  1 | Inception      |                      0 |                      0 |   ...               |            0.367959 |      2 |
    |  2 | Frankenstein   |                      0 |                      0 |   ...               |            0.136419 |      5 |
    |  3 | Wizard Spider  |                      0 |                      0 |   ...               |            0.269734 |      4 |
    |  4 | Cobalt Group   |                      0 |                      0 |   ...               |            0.31689  |      3 |
    +----+----------------+------------------------+------------------------+---------------------+---------------------+--------+


    # Ranking WASPAS
    threats_waspas.ranking_by_chain_severity(threat_series_severity, "WASPAS", alternatives)

    # Ranking PROMETHEE_II
    threats_promethee_ii.ranking_by_chain_severity(threat_series_severity, "PROMETHEE_II", alternatives)

    print(threats_topsis.weights)
    [0.09245283018867927, 
     ...
     0.07358490566037737, 
     0.0]

    # Ranking ENSEMBLE
    threats_voting_soft = ENSEMBLE()
    threats_voting_soft.ranking_by_voting([threats_waspas.method, threats_topsis.method, threats_promethee_ii.method])
    print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))

    +----+----------------+-----------+----------+----------------+-----------+--------+
    |    | alternatives   |    WASPAS |   TOPSIS |   PROMETHEE_II |    voting |   rank |
    |----+----------------+-----------+----------+----------------+-----------+--------|
    |  0 | APT41          | 0.3       | 0.531377 |      0.0551887 | 0.295522  |      1 |
    |  1 | Inception      | 0.161321  | 0.367959 |     -0.0367925 | 0.164163  |      2 |
    |  2 | Frankenstein   | 0.0367925 | 0.136419 |     -0.0367925 | 0.0454729 |      5 |
    |  3 | Wizard Spider  | 0.0471698 | 0.269734 |     -0.0367925 | 0.0933706 |      4 |
    |  4 | Cobalt Group   | 0.108962  | 0.31689  |      0.0551887 | 0.160347  |      3 |
    +----+----------------+-----------+----------+----------------+-----------+--------+

6. Test of RISK ranking using ENSEMBLE (Voting)

.. code-block:: python
    
    from scikitmcda.ensemble import ENSEMBLE 
    from tabulate import tabulate
    from e12lib.priority import Priority
    
    # Test of risk
    threats_topsis = Priority()
    threats_waspas = Priority()
    threats_promethee_ii = Priority()

    threat_series_severity = [["CVE-2019-19781", "CVE-2020-10189", 
                               "CVE-2019-19781", "CVE-2020-10189", 
                               "CVE-2019-19781", "CVE-2012-0158",  
                               "CVE-2015-1641", "CVE-2017-0199",  
                               "CVE-2017-11882", "CVE-2019-3396"], # 1. APT41
                              ["CVE-2012-0158", "CVE-2014-1761", 
                               "CVE-2017-11882", "CVE-2018-0802"], # 2. Inception
                              ["CVE-2017-11882"],                  # 3. Frankenstein
                              ["CVE-2020-1472", "MS17-010"],       # 4. Wizard Spider
                              ["CVE-2018-8174", "CVE-2017-8570", 
                               "CVE-2017-0199", "CVE-2017-875"]]   # 5. Cobalt Group 

    alternatives = ["APT41", "Inception", "Frankenstein", "Wizard Spider", "Cobalt Group"]

    # Ranking with TOPSIS
    threats_topsis.ranking_by_chain_risk(threat_series_severity, alt_labels=alternatives)
    print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))

    +----+----------------+--------------+----------+-----------+--------+
    |    | alternatives   |   likelihood |   impact |      risk |   rank |
    |----+----------------+--------------+----------+-----------+--------|
    |  0 | APT41          |     0.58536  | 0.493617 | 0.288944  |      1 |
    |  1 | Inception      |     0.201003 | 0.311164 | 0.0625451 |      4 |
    |  2 | Frankenstein   |     0.10945  | 0.173786 | 0.0190209 |      5 |
    |  3 | Wizard Spider  |     0.33989  | 0.280073 | 0.0951941 |      3 |
    |  4 | Cobalt Group   |     0.252309 | 0.405636 | 0.102346  |      2 |
    +----+----------------+--------------+----------+-----------+--------+

    # Ranking WASPAS
    threats_waspas.ranking_by_chain_risk(threat_series_severity, "WASPAS", alternatives)

    # Ranking PROMETHEE_II
    threats_promethee_ii.ranking_by_chain_risk(threat_series_severity, "PROMETHEE_II", alternatives)

    # Ranking ENSEMBLE
    threats_voting_soft = ENSEMBLE()
    threats_voting_soft.ranking_by_voting([threats_waspas.method, threats_topsis.method, threats_promethee_ii.method])
    print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))

    +----+----------------+------------+-----------+----------------+-----------+--------+
    |    | alternatives   |     WASPAS |    TOPSIS |   PROMETHEE_II |    voting |   rank |
    |----+----------------+------------+-----------+----------------+-----------+--------|
    |  0 | APT41          | 0.0869265  | 0.288944  |     0.172236   | 0.182702  |      1 |
    |  1 | Inception      | 0.00818131 | 0.0625451 |     0.00676095 | 0.0258291 |      4 |
    |  2 | Frankenstein   | 0.00204533 | 0.0190209 |     0.0414612  | 0.0208425 |      5 |
    |  3 | Wizard Spider  | 0.00450666 | 0.0951941 |     0.0202833  | 0.0399947 |      2 |
    |  4 | Cobalt Group   | 0.0177262  | 0.102346  |    -0.00322014 | 0.0389506 |      3 |
    +----+----------------+------------+-----------+----------------+-----------+--------+