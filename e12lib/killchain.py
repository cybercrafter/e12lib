#########################################################
# E12Lib - killchain                                    #
# Author: Antonio Horta <ajhorta@cybercrafter.com.br>   #
# https://gitlab.com/cybercrafter/e12lib                #
# Cybercrafter ® 2021                                   #
#########################################################

import sys
from .constant import *
from .kdd import KDDSec, Knowledge
import datetime
import hashlib
import numpy as np


class Polymer:
    def __init__(self, data):  # constructor method
        """
        Polymer Constructor.

        Reference:

        Horta,  A.  and  Santos,  A.  (2021).   Polymer: An Adaptive Kill 
            Chain Expanding High-Level Cyber Threat Hunting to Multi-Platform 
            Environments. In 2021 IoTBDS International Conferenceon on 
            Internet of Things, Big Data and Security, page 22. INSTICC.

        :param data: Knowledge obtained from KDDSec
        :type data: Knowledge
        """
        if type(data) == Knowledge:
            if ("ATT&CK-v" + str(POLYMER_VERSION) + ".") not in data.attack_version:
                print("Incompatible ATT&CK and Polymer versions")
                sys.exit(1)
            self.version = POLYMER_VERSION
            self.attack_version = data.attack_version
            self.phases = data.tactics
            self.techniques = self.__get_techniques(data.techniques)
            self.platforms = self.__get_platforms(data.techniques)
            self.binary_table = data.binary_table
        else:
            print("Invalid data initializing Polymer")
            sys.exit(1)

    def __get_platforms(self, techniques):
        platforms = []
        for t in techniques:
            for p in t['x_mitre_platforms']:
                item = {"platform": p, "source_repo": t['source_repo']}
                if item not in platforms:
                    platforms.append(item)
        return platforms

    def __get_techniques(self, techniques):
        tech = []
        for t in techniques:
            for p in [t['kill_chain_phases']]:
                higher_pos = 0
                pos = 0
                attack_tactic = ""
                for i in p:
                    pos = next((item['position'] for item in self.phases if item["attack_tactic"] == i), None)
                    if pos > higher_pos:
                        attack_tactic = i
                    for eid in t['external_id']:
                        item = {"attack_tactic": attack_tactic, "external_id": eid, "impact": pos}
                        if item not in tech:
                            tech.append(item)
        return tech

class Link:
    def __init__(self, polymer, external_id, platform, date_time=None, log_id=None):  # constructor method
        """
        Link Constructor.
        :param polymer: Polymer instance
        :type polymer: Polymer

        :param external_id: Technique ATTACK or CAPEC id. e.g. T1553 or CAPEC-253
        :type external_id: string

        :param platform: Platform available in Polymer version. e.g: Android, Windows etc
        :type platform: string

        :param date_time: Date time of event
        :type date_time: string

        :param log_id: Log id
        :type log_id: string

        """
        if type(polymer) == Polymer:
            t = next((item for item in polymer.techniques if external_id in item["external_id"]), None)
            p = next((item for item in polymer.platforms if item["platform"].lower() == platform.lower()), None)
            try:
                self.external_id = external_id
                self.impact = t["impact"]
                self.platform = p["platform"]
                self.source_repo = p["source_repo"]
                self.date_time = date_time
                self.log_id = log_id
                self.polymer = polymer
            except Exception as e:
                print("Incompatible data inserted into link")

class Chain:
    def __init__(self, links):  # constructor method
        """
        Chain Constructor.
        
        :param links: list of links by threat
        :type links: list of Link
        """
        try:
            self.chain_id = "attack-chaining--" + hashlib.md5(str(links).encode('utf-8')).hexdigest()
            self.chain = links
            self.c2 = self.__get_c2(links)
            self.c3 = self.__get_c3(links)
            self.c5 = self.__get_c5(links)
            self.binary_table_row = self.__get_chain_binary_table_row()
        except Exception as e:
            print("Incompatible link inserted into chain")

    def __get_chain_binary_table_row(self):
        
        data = np.zeros(len(self.chain[0].polymer.binary_table.columns.tolist())-2)
        for l in self.chain:
            i = l.polymer.binary_table.columns.tolist().index(l.external_id) -2
            data[i] = 1
        row = [self.chain_id, None]
        row = row + data.tolist()
        return row

    def __get_c2(self, links):
        distinct = []
        for l in links:
            if l.impact not in distinct:
                distinct.append(l.impact)
        return len(distinct)

    def __get_c3(self, links):
        higher_impact = 0
        for l in links:
            if l.impact > higher_impact:
                higher_impact = l.impact
        return higher_impact

    def __get_c5(self, links):
        distinct = []
        for l in links:
            if l.external_id not in distinct:
                distinct.append(l.external_id)
        return len(distinct)
