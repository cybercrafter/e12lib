import setuptools

with open("README.rst", "r") as fh:
    long_description = fh.read()


setuptools.setup(
      name="e12lib",
      version="8.2.24",
      author="Antonio Horta",
      author_email='ajhorta@cybercrafter.com.br',
      description="Python Library to perform high level cyber threat hunting using polymer kill chain.",
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://gitlab.com/cybercrafter/e12lib",
      install_requires=[
            "numpy~=1.18",
            "pandas~=1.0",
            "stix2",
            "gitpython",
            "sklearn",
            "scikit-mcda",
            ],
      license="Apache License 2.0",
      packages=setuptools.find_packages(),
      classifiers=[
        "Programming Language :: Python",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries",
      ],
      python_requires='>=3.7',
      )
