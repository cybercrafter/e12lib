#########################################################
# E12Lib - kdd                                          #
# Author: Antonio Horta <ajhorta@cybercrafter.com.br>   #
# https://gitlab.com/cybercrafter/e12lib                #
# Cybercrafter ® 2021                                   #
#########################################################

import os
import sys
from git import GitCommandError, GitCmdObjectDB
from git import Repo
from git.cmd import Git
from .constant import *
from stix2 import Filter, FileSystemSource
import pandas as pd
import numpy as np
import requests
from scikitmcda.topsis import TOPSIS 
from scikitmcda.vikor import VIKOR
from scikitmcda.waspas import WASPAS
from scikitmcda.wpm import WPM
from scikitmcda.wsm import WSM
from scikitmcda.electre_ii import ELECTRE_II
from scikitmcda.promethee_ii import PROMETHEE_II
from scikitmcda.ensemble import ENSEMBLE 

class Knowledge:
    def __init__(self, data):
        self.attack_version = data["attack_version"]
        self.tactics = data["tactics"]
        self.techniques = data["techniques"]
        self.mitigations = data["mitigations"]
        self.groups = data["groups"]
        self.malwares = data["malwares"]
        self.tools = data["tools"]
        self.procedures = data["procedures"]
        self.relationships = data["relationships"]
        self.binary_table = data["binary_table"]

class KDDSec:
    """
    Class to perform KDDSec (Knowledge Discover in Database for Security).
    
    Reference: 
    
    Horta,  A.  and  Santos,  A.  (2020).    Cyber  threat  hunting
        through automated hypothesis and multi-criteria deci-sion making.  
        In 2020 IEEE International Conferenceon Big Data, page 22. IEEE.
    """
    def __init__(self):  # constructor method
        self.polymer_version = POLYMER_VERSION

    def update(self):
        """
        Download or update Mitre CTI and do the checkout to the last compatible version of POLYMER
        """

        repo_dir = os.path.join(os.path.dirname(__file__), REPO_FOLDER)  # get current directory
        git_url = "https://github.com/mitre/cti.git"

        try:
            if os.path.isdir(repo_dir):
                    # pull
                    repo = Repo(repo_dir)
                    repo.head.reference = repo.heads.master
                    repo.heads.master.checkout()
                    g = Git(repo_dir)
                    g.pull()
            else:
                    # clone
                    Repo.clone_from(git_url, repo_dir)

        except GitCommandError as ex:
            print("Git error. Unable to connect repository: ", ex)

        return True

    def load(self, version=POLYMER_VERSION):
        """
        Load ALL knowledge obtained by KDDSec process

        :param version: Version of polymer. Default is the last Polymer version
        :type version: int

        :return: Mitre ATT&CK Elements
        :return type: list of dict
        """

        categories = []
        tactics = []

        if version >= 8 and version < 9:
            tactics = TACTICS_V8
        else:
            try:
                raise Exception('PolymerVersionError')
            except Exception as e:
                raise ValueError("Oops! ", version, "is not a valid polymer's version.  Try another one or empty.")

        t = self.__checkout(version)
        mitreattackelements = self.__discover(t, tactics)

        return mitreattackelements

    def __checkout(self, version=POLYMER_VERSION):
        repo_dir = os.path.join(os.path.dirname(__file__), REPO_FOLDER)  # get current directory
        if os.path.isdir(repo_dir):
                # checkout
                repo = Repo(repo_dir)
                repo.head.reference = repo.heads.master
                repo.heads.master.checkout()
                try:
                    repo.delete_head("polymer_last")
                except Exception as e:
                    pass
                t = self.__get_last_version(repo_dir, version)
                polymer_last = repo.tags[t]
                repo.create_head("polymer_last", polymer_last)
                repo.heads.polymer_last.checkout(force=True)

        else:
                # create
                self.update()
                self.__checkout(version)
                t = self.__get_last_version(repo_dir, version)

        return t

    def __get_last_version(self, repo_dir, version=POLYMER_VERSION):
        """
        Get last compatible ATT&CK tag compatible with POLYMER_VERSION of E12Lib
        """

        tag_sufix = "ATT&CK-v" + str(version) + "."

        if "." in str(version):
            return "ATT&CK-v" + str(version)
        else:
            repo = Repo(repo_dir, odbt=GitCmdObjectDB)
            version_tags = []
            for t in repo.tags:
                if tag_sufix in str(t):
                    version_tags.append(t.path.replace("refs/tags/" + tag_sufix, ""))
            return tag_sufix + max(version_tags)

    def __discover(self, tag, tactics):

        repo_dir = os.path.join(os.path.dirname(__file__), REPO_FOLDER)  # get current directory

        # WORKAROUND ERRORS MITRE INCONSISTENCE
        errors = [os.path.join(repo_dir, "ics-attack", "relationship", "relationship--8j9f95f0-4939-4e74-9073-70efddddff50.json"),
                  os.path.join(repo_dir, "ics-attack", "relationship", "relationship--a9c632fe0-2619-4e1a-a04b-018000644a0f.json")
                  ]

        for e in errors:
            try:
                os.remove(e)
                print("File corrupted found was removed :", e)
            except Exception as e:
                pass

        techniques = []
        mitigations = []
        groups = []
        malwares = []
        tools = []
        relationships = []

        for dirs in MITRE_CTI_V8:
            tc_path = os.path.join(repo_dir, dirs)

            tc_source = FileSystemSource(tc_path)

            # Create filters to retrieve content from Enterprise ATT&CK
            filter_objs = {"techniques": Filter("type", "=", "attack-pattern"),
                           "mitigations": Filter("type", "=", "course-of-action"),
                           "groups": Filter("type", "=", "intrusion-set"),
                           "malwares": Filter("type", "=", "malware"),
                           "tools": Filter("type", "=", "tool"),
                           "relationships": Filter("type", "=", "relationship")
                           }

            # Attack Pattern update
            elements = tc_source.query(filter_objs["techniques"])
            techniques = techniques + self.__mitre_attack_elements_update(elements, dirs)

            # IntrusionSet update
            elements = tc_source.query(filter_objs["groups"])
            groups = groups + self.__mitre_attack_elements_update(elements, dirs)

            # Course of Action update
            elements = tc_source.query(filter_objs["mitigations"])
            mitigations = mitigations + self.__mitre_attack_elements_update(elements, dirs)

            # Malware update
            elements = tc_source.query(filter_objs["malwares"])
            malwares = malwares + self.__mitre_attack_elements_update(elements, dirs)

            # Tools update
            elements = tc_source.query(filter_objs["tools"])
            tools = tools + self.__mitre_attack_elements_update(elements, dirs)

            # Relationship update
            elements = tc_source.query(filter_objs["relationships"])
            relationships = relationships + self.__relationships_update(elements, dirs)

            # if "enterprise-attack" in str(tc_path):
                # Sigmarules update
                # sigma.download_sigma_rules()
                # sigma_rules = sigma.import_sigma_rules()
                # sigma_rules_update(sigma_rules)

        techniques = self.__distinct_techniques(techniques)[0]
        mitigations = self.__distinct_elements(mitigations)
        groups = self.__distinct_elements(groups)
        malwares = self.__distinct_elements(malwares)
        tools = self.__distinct_elements(tools)
        # discovering procedures
        procedures = self.__discovering_procedures((groups + malwares + tools),
                                                   relationships,
                                                   techniques)
        binary_table = self.__build_binary_table(procedures,
                                                 self.__distinct_techniques(techniques)[1])
        return Knowledge({"attack_version": tag,
                          "tactics": tactics,
                          "techniques": techniques,
                          "mitigations": mitigations,
                          "groups": groups,
                          "malwares": malwares,
                          "tools": tools,
                          "procedures": procedures,
                          "relationships": relationships,
                          "binary_table": binary_table})

    def __discovering_procedures(self, attackers, relationships, techniques):

        procedures = []

        for p in attackers:
            attack_patterns = []
            for e in relationships:
                if e['relationship_type'] == "uses" and p['mitre_attack_element_id'] == e['source_ref']:
                    tech = next((item for item in techniques if item["mitre_attack_element_id"] == e["target_ref"]), None)
                    if tech is not None:
                        attack_patterns.append(tech['external_id'])
            procedures.append({'procedure_id': p['mitre_attack_element_id'], 'mitre_attack_element_type': p['mitre_attack_element_type'], 'techniques': attack_patterns})

        return procedures

    def __build_binary_table(self, procedures, techniques):

        df = pd.DataFrame(np.zeros((len(procedures),
                          len(techniques) + 2)),
                          columns=["procedure_id", "type"] +
                          [sub for sub in techniques])

        for p in procedures:
            i = procedures.index(p)
            df.iloc[i, 0] = p['procedure_id']
            df.iloc[i, 1] = p['mitre_attack_element_type']
            for t in p['techniques']:
                if t in list(df.columns):
                    c = list(df.columns).index(t)
                    df.iloc[i, c] = 1
        return df

    def __distinct_elements(self, elements):
        items = []
        unique = []

        for e in elements:
            if e['name'] not in items:
                items.append(e['name'])
                unique.append(e)

        return unique

    def __distinct_techniques(self, techniques):
        items = []
        unique = []

        for e in techniques:
            for t in e['external_id']:
                if t not in items:
                    items.append(t)
                    unique.append(e)

        return unique, items

    def __mitre_attack_elements_update(self, elements, dirs):

        mitreattackelements = []

        for i in elements:
            if 'description' in i:
                description = i.description
            else:
                description = None

            # WORKAROUND IntegrityError at MITRE ICS
            fixed_id = i.id
            duplicated = ['intrusion-set--32bca8ff-d900-4877-aa65-d70baa041b74',
                          'malware--a020a61c-423f-4195-8c46-ba1d21abba37']
            if i.id in duplicated:
                for icssrc in i.external_references:
                    if 'source_name' in icssrc:
                        if 'mitre-ics-attack' in icssrc.source_name:
                            fixed_id = i.id + '-ICS'

            mitreattackelement = {'source_repo': dirs,
                                  'mitre_attack_element_id': fixed_id,
                                  'mitre_attack_element_type': i.type,
                                  'name': i.name,
                                  'description': description
                                  }

            external_references = self.__external_references_update(i)
            mitreattackelement["external_id"] = external_references[0]
            mitreattackelement["external_references"] = external_references[1]
            mitreattackelement["kill_chain_phases"] = self.__kill_chain_phase_update(i)
            mitreattackelement["x_mitre_platforms"] = self.__x_mitre_platform_update(i)
            mitreattackelement["data_sources"] = self.__data_source_update(i)
            mitreattackelement["x_mitre_defense_bypassed"] = self.__x_mitre_defense_bypassed_update(i)
            mitreattackelement["x_mitre_impact_type"] = self.__x_mitre_impact_type_update(i)

            mitreattackelements.append(mitreattackelement)

        return mitreattackelements

    def __external_references_update(self, i):

        externalreferences = []

        if 'external_references' in i:
            external_id_ = []
            for er in i.external_references:
                if 'url' in er:
                    url = er.url
                    # Constraint Bug Mitre - issue opened to Mitre
                    if er.url.startswith('https://attack.mitre.org/mitigations/T'):
                        url = 'https://attack.mitre.org/techniques/' + er.external_id
                else:
                    url = None
                if 'description' in er:
                    description = er.description
                else:
                    description = None
                if 'external_id' in er:
                    # if er.external_id[0] == "T":
                    #    external_id_ = er.external_id
                    external_id_.append(er.external_id)
                    external_id = er.external_id
                    # WORKAROUND for beta subtechniques: to correct url links on cti beta subtechniques branch
                    # if '.' in external_id:
                    #     url = url.replace('https://attack.mitre.org/techniques/', 'https://attack.mitre.org/beta/techniques/')
                    # WORKAROUND ICS-ATTACK DUPLICATIONS
                    if 'mitre-ics-attack' in er.source_name:
                        if external_id in url:
                            # if er.external_id[0] == "T":
                            #    external_id_ = external_id + '-ICS'
                            external_id_.append(er.external_id + '-ICS')
                            external_id = external_id + '-ICS'
                        else:
                            if 'https://collaborate.mitre.org/attackics/index.php/Group/' in url:
                                external_id = url.replace('https://collaborate.mitre.org/attackics/index.php/Group/', '') + '-ICS'
                            if 'https://collaborate.mitre.org/attackics/index.php/Technique/' in url:
                                external_id = url.replace('https://collaborate.mitre.org/attackics/index.php/Technique/', '') + '-ICS'
                else:
                    external_id = None
                if 'source_name' in er:
                    externalreference = {'source_name': er.source_name,
                                         'url': url,
                                         'external_id': external_id,
                                         'description': description}

                    externalreferences.append(externalreference)

        return external_id_, externalreferences

    def __kill_chain_phase_update(self, i):

        killchainphases = []

        if 'kill_chain_phases' in i:
            for kc in i.kill_chain_phases:
                if 'phase_name' in kc:
                    # concat ICS tactics to form polymer
                    if 'evasion' in kc.phase_name:
                        phase_name = 'defense-evasion'
                    else:
                        phase_name = kc.phase_name.replace('-ics', '')
                else:
                    phase_name = None

                killchainphases.append(phase_name)

        return killchainphases

    def __x_mitre_platform_update(self, i):

        xmitreplatforms = []

        if 'x_mitre_platforms' in i:
            for ap in i.x_mitre_platforms:
                name = ap

                xmitreplatforms.append(name)

        return xmitreplatforms

    def __x_mitre_impact_type_update(self, i):

        xmitreimpacttype = []

        if 'x_mitre_impact_type' in i:
            for ap in i.x_mitre_impact_type:
                name = ap

                xmitreimpacttype.append(name)

        return xmitreimpacttype

    def __data_source_update(self, i):

        datasources = []

        if 'x_mitre_data_sources' in i:
            for ap in i.x_mitre_data_sources:
                name = ap

                datasources.append(name)

        return datasources

    def __x_mitre_defense_bypassed_update(self, i):

        xmitredefensepypassed = []

        if 'x_mitre_defense_bypassed' in i:
            for ap in i.x_mitre_defense_bypassed:
                name = ap

                xmitredefensepypassed.append(name)

        return xmitredefensepypassed

    def __relationships_update(self, elements, dirs):

        relationships = []

        for i in elements:

            if 'description' in i:
                description = i.description
            else:
                description = None

            if 'relationship_type' in i:
                relationship_type = i.relationship_type
            else:
                relationship_type = None

            if 'target_ref' in i:
                target_ref = i.target_ref
            else:
                target_ref = None

            if 'source_ref' in i:
                source_ref = i.source_ref
            else:
                source_ref = None

            relationships.append({'source_repo': dirs,
                                  'target_ref': target_ref,
                                  'source_ref': source_ref,
                                  'description': description,
                                  'relationship_type': relationship_type})

        return relationships
    


