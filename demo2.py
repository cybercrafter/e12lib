from scikitmcda.ensemble import ENSEMBLE 
from tabulate import tabulate
from e12lib.priority import Priority

# Test of severity
threats_topsis = Priority()
threats_waspas = Priority()
threats_promethee_ii = Priority()

threat_series_severity = [["CVE-2019-19781", "CVE-2020-10189", 
                           "CVE-2019-19781", "CVE-2020-10189", 
                           "CVE-2019-19781", "CVE-2012-0158",  
                           "CVE-2015-1641", "CVE-2017-0199",  
                           "CVE-2017-11882", "CVE-2019-3396"], # 1. APT41
                          ["CVE-2012-0158", "CVE-2014-1761", 
                           "CVE-2017-11882", "CVE-2018-0802"], # 2. Inception
                          ["CVE-2017-11882"],                  # 3. Frankenstein
                          ["CVE-2020-1472", "MS17-010"],       # 4. Wizard Spider
                          ["CVE-2018-8174", "CVE-2017-8570", 
                           "CVE-2017-0199", "CVE-2017-875"]]   # 5. Cobalt Group 


alternatives = ["APT41", "Inception", "Frankenstein", "Wizard Spider", "Cobalt Group"]

 # Ranking with TOPSIS
threats_topsis.ranking_by_chain_risk(threat_series_severity, alt_labels=alternatives)
print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))

# Ranking WASPAS
threats_waspas.ranking_by_chain_risk(threat_series_severity, "WASPAS", alternatives)

# Ranking PROMETHEE_II
threats_promethee_ii.ranking_by_chain_risk(threat_series_severity, "PROMETHEE_II", alternatives)

# Ranking ENSEMBLE
threats_voting_soft = ENSEMBLE()
threats_voting_soft.ranking_by_voting([threats_waspas.method, threats_topsis.method, threats_promethee_ii.method])
print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))
