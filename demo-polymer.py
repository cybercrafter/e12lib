################################################
#
# Polymer's demo for blind Review 
#
# Paper: Expanding High-Level Threat Hunting to
#        Multi-Platform Environments though the
#        Polymer Kill Chain and Entropy
#  
################################################

from e12lib.kdd import KDDSec
from e12lib.hlcth import Screener
from e12lib.killchain import Polymer, Link, Chain
import pprint
from tabulate import tabulate
from e12lib.priority import Priority

# Proceed KDDSec to pull data from Mitre
kddsec = KDDSec()

# Load knowledge after KDDSec
knowledge = kddsec.load()

# Show kwnoledge
print("ATT&CK Version: ", knowledge.attack_version)
print("Tactics: ", len(knowledge.tactics))
print("Techniques (distinct): ", len(knowledge.techniques))
print("Mitigations: ", len(knowledge.mitigations))
print("Groups: ", len(knowledge.groups))
print("Malwares: ", len(knowledge.malwares))
print("Tools: ", len(knowledge.tools))
print("Procedures: ", len(knowledge.procedures))
print("Relationships: ", len(knowledge.relationships))
print("Binary Table:\n", knowledge.binary_table)

# Build polymer by knowledge 
polymer = Polymer(knowledge)

print("\n\nPolymer Version: ", polymer.version)
print("ATT&CK Version: ", polymer.attack_version)
print("Platforms: ", len(polymer.platforms))
print("Techniques (not unique): ", len(polymer.techniques))
print("Polymer phases: ", len(polymer.phases))
print("Binary Table (shape): ", polymer.binary_table.shape)

# Init screener with polymer to proceed high-level cyber threat hunting
screener = Screener(polymer)

# Training different strategies
model_fit_MLP = screener.training(False, "MLPClassifier")
model_fit_RDF_clustering = screener.training(True, "RandomForestClassifier")

print("\n\nModel\n")
pprint.pprint(model_fit_MLP)
pprint.pprint(model_fit_RDF_clustering)

# DarkHydrus
threat = []
threat.append(Link(polymer, "T1564.003", "Windows"))
threat.append(Link(polymer, "T1187", "Linux"))
threat.append(Link(polymer, "T1221", "Linux"))
threat.append(Link(polymer, "T1204.002", "Linux"))
threat.append(Link(polymer, "T1059.001", "Linux"))
threat.append(Link(polymer, "T1566.001", "Linux"))
threat.append(Link(polymer, "CAPEC-163", "Windows"))
threat_chaining_1 = Chain(threat)

# KillDisk
threat = []
threat.append(Link(polymer, "T0881", "Data Historian"))
threat.append(Link(polymer, "T0872", "Data Historian"))
threat.append(Link(polymer, "T0829", "Data Historian"))
threat.append(Link(polymer, "T0809", "Data Historian"))
threat_chaining_2 = Chain(threat)

# AndroRAT
threat = []
threat.append(Link(polymer, "T1433", "Android"))
threat.append(Link(polymer, "T1432", "Android"))
threat.append(Link(polymer, "T1429", "Android"))
threat.append(Link(polymer, "T1412", "Android"))
threat.append(Link(polymer, "T1430", "Android"))
threat_chaining_3 = Chain(threat)

threat_series = [threat_chaining_1, threat_chaining_2, threat_chaining_3]

print("\nDarkHydrus is ", threat_chaining_1.chain_id)
print("KillDisk is ", threat_chaining_2.chain_id)
print("AndroRAT is ", threat_chaining_3.chain_id)

print("\n\nHLCTH NO CLUSTERING STRATEGY with Polymer and Original Weights\n") 

ranking_no_cluster_original_weights_TOPSIS = screener.rank(threat_series, model_fit_MLP, mcda_alg="TOPSIS", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_no_cluster_original_weights_VIKOR = screener.rank(threat_series, model_fit_MLP, mcda_alg="VIKOR", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_no_cluster_original_weights_WASPAS = screener.rank(threat_series, model_fit_MLP, mcda_alg="WASPAS", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_no_cluster_original_weights_ELECTRE_II = screener.rank(threat_series, model_fit_MLP, mcda_alg="ELECTREII", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_no_cluster_original_weights_PROMETHEE_II = screener.rank(threat_series, model_fit_MLP, mcda_alg="PROMETHEEII", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)

pprint.pprint(ranking_no_cluster_original_weights_TOPSIS)
pprint.pprint(ranking_no_cluster_original_weights_VIKOR)
pprint.pprint(ranking_no_cluster_original_weights_WASPAS)
pprint.pprint(ranking_no_cluster_original_weights_ELECTRE_II)
pprint.pprint(ranking_no_cluster_original_weights_PROMETHEE_II)

print("\n\nHLCTH CLUSTERING STRATEGY with Polymer and Original Weights\n") 

ranking_cluster_original_weights_TOPSIS = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="TOPSIS", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_cluster_original_weights_VIKOR = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="VIKOR", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_cluster_original_weights_WASPAS = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="WASPAS", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_cluster_original_weights_ELECTRE_II = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="ELECTREII", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)
ranking_cluster_original_weights_PROMETHEE_II = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="PROMETHEEII", wc1=0.40, wc2=0.10, wc3=0.10, wc4=0.10, wc5=0.30)

pprint.pprint(ranking_cluster_original_weights_TOPSIS)
pprint.pprint(ranking_cluster_original_weights_VIKOR)
pprint.pprint(ranking_cluster_original_weights_WASPAS)
pprint.pprint(ranking_cluster_original_weights_ELECTRE_II)
pprint.pprint(ranking_cluster_original_weights_PROMETHEE_II)

print("\n\nHLCTH NO CLUSTERING STRATEGY with Polymer and Weights by Entropy\n") 

ranking_no_cluster_entropy_TOPSIS = screener.rank(threat_series, model_fit_MLP, mcda_alg="TOPSIS")
ranking_no_cluster_entropy_VIKOR = screener.rank(threat_series, model_fit_MLP, mcda_alg="VIKOR")
ranking_no_cluster_entropy_WASPAS = screener.rank(threat_series, model_fit_MLP, mcda_alg="WASPAS")
ranking_no_cluster_entropy_ELECTRE_II = screener.rank(threat_series, model_fit_MLP, mcda_alg="ELECTREII")
ranking_no_cluster_entropy_PROMETHEE_II = screener.rank(threat_series, model_fit_MLP, mcda_alg="PROMETHEEII")

pprint.pprint(ranking_no_cluster_entropy_TOPSIS)
pprint.pprint(ranking_no_cluster_entropy_VIKOR)
pprint.pprint(ranking_no_cluster_entropy_WASPAS)
pprint.pprint(ranking_no_cluster_entropy_ELECTRE_II)
pprint.pprint(ranking_no_cluster_entropy_PROMETHEE_II)

print("\n\nHLCTH CLUSTERING STRATEGY with Polymer and Weights by Entropy\n") 

ranking_cluster_entropy_TOPSIS = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="TOPSIS")
ranking_cluster_entropy_VIKOR = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="VIKOR")
ranking_cluster_entropy_WASPAS = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="WASPAS")
ranking_cluster_entropy_ELECTRE_II = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="ELECTREII")
ranking_cluster_entropy_PROMETHEE_II = screener.rank(threat_series, model_fit_RDF_clustering, mcda_alg="PROMETHEEII")

pprint.pprint(ranking_cluster_entropy_TOPSIS)
pprint.pprint(ranking_cluster_entropy_VIKOR)
pprint.pprint(ranking_cluster_entropy_WASPAS)
pprint.pprint(ranking_cluster_entropy_ELECTRE_II)
pprint.pprint(ranking_cluster_entropy_PROMETHEE_II)


# Output
"""

ATT&CK Version:  ATT&CK-v8.2
Tactics:  18
Techniques (distinct):  1002
Mitigations:  294
Groups:  114
Malwares:  463
Tools:  66
Procedures:  643
Relationships:  10718
Binary Table:
                                           procedure_id           type  T1564.006  T1016  CAPEC-309  T1584.004  T1080  ...  T0882  T0863  T0800  T0858  T0861  T0823  T0828
0    intrusion-set--96e239be-ad99-49eb-b127-3007b8c...  intrusion-set        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
1    intrusion-set--6713ab67-e25b-49cc-808d-2b36d4f...  intrusion-set        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
2    intrusion-set--59140a2e-d117-4206-9b2c-2a8662b...  intrusion-set        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
3    intrusion-set--1f21da59-6a13-455b-afd0-d58d0a5...  intrusion-set        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
4    intrusion-set--fe8796a4-2a02-41a0-9d27-7aa1e99...  intrusion-set        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
..                                                 ...            ...        ...    ...        ...        ...    ...  ...    ...    ...    ...    ...    ...    ...    ...
638         tool--9a2640c2-9f43-46fe-b13f-bde881e55555           tool        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
639         tool--da04ac30-27da-4959-a67d-450ce47d9470           tool        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
640         tool--cb69b20d-56d0-41ab-8440-4a4b251614d4           tool        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
641         tool--1622fd3d-fcfc-4d02-ac49-f2d786f79b81           tool        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0
642         tool--da21929e-40c0-443d-bdf4-6b60d15448b4           tool        0.0    0.0        0.0        0.0    0.0  ...    0.0    0.0    0.0    0.0    0.0    0.0    0.0

[643 rows x 1004 columns]


Polymer Version:  8
ATT&CK Version:  ATT&CK-v8.2
Platforms:  35
Techniques (not unique):  1130
Polymer phases:  18
Binary Table (shape):  (643, 1004)

Model TRAINIG
=============

{'algorithm': 'MLPClassifier',
 'clustering': False,
 'model': Pipeline(steps=[('clf', MLPClassifier(alpha=1, max_iter=1000, random_state=0))]),
 'prob_mean': 0.7076129768213053,
 'scatter': array([[0., 0.],
       [0., 0.],
       [0., 0.],
       ...,
       [0., 0.],
       [0., 0.],
       [0., 0.]]),
 'scores_mean': 0.7184939091915835}

{'algorithm': 'RandomForestClassifier',
 'clustering': True,
 'model': Pipeline(steps=[('clf',
                 RandomForestClassifier(n_estimators=45, random_state=0))]),
 'prob_mean': 0.9983779819571881,
 'scatter': array([[0., 0.],
       [0., 0.],
       [0., 0.],
       ...,
       [0., 0.],
       [0., 0.],
       [0., 0.]]),
 'scores_mean': 0.9984126984126984}

==================================================================
DarkHydrus is  attack-chaining--6821aa25ad4e38227f2bc991fd014bc4
KillDisk is  attack-chaining--0f1283e4600794736e61b11126589a3b
AndroRAT is  attack-chaining--8be7a62f94f84111854cb34adad5e514
==================================================================

HLCTH NO CLUSTERING STRATEGY with Polymer and Original Weights
==============================================================

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': False,
 'mcda': 'TOPSIS',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.72'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.41'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.32'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': False,
 'mcda': 'VIKOR',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '1.00'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.25'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.25'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': False,
 'mcda': 'WASPAS',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '1.95'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '1.90'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '1.74'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': False,
 'mcda': 'ELECTREII',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.83'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.67'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.50'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': False,
 'mcda': 'PROMETHEEII',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.21'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.00'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '-0.21'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}


HLCTH CLUSTERING STRATEGY with Polymer and Original Weights
===========================================================

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': False,
 'mcda': 'TOPSIS',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.72'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.41'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.32'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': False,
 'mcda': 'VIKOR',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '1.00'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.25'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.08'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': False,
 'mcda': 'WASPAS',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '1.97'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '1.91'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '1.75'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': False,
 'mcda': 'ELECTREII',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.83'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.58'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': False,
 'mcda': 'PROMETHEEII',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.22'},
             {'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '-0.08'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '-0.14'}],
 'weights': [0.4, 0.1, 0.1, 0.1, 0.3]}



HLCTH NO CLUSTERING STRATEGY with Polymer and Weights by Entropy
================================================================

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': True,
 'mcda': 'TOPSIS',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.90'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.77'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.15'}],
 'weights': [1.1589190107833798e-15,
             0.634943835072813,
             0.23734374636777902,
             1.5797862441444182e-10,
             0.1277124184014282]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': True,
 'mcda': 'VIKOR',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '1.00'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.81'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.00'}],
 'weights': [1.1589190107833798e-15,
             0.634943835072813,
             0.23734374636777902,
             1.5797862441444182e-10,
             0.1277124184014282]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': True,
 'mcda': 'WASPAS',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '3.66'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '2.67'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '2.30'}],
 'weights': [1.1589190107833798e-15,
             0.634943835072813,
             0.23734374636777902,
             1.5797862441444182e-10,
             0.1277124184014282]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': True,
 'mcda': 'ELECTREII',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.75'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.50'}],
 'weights': [1.1589190107833798e-15,
             0.634943835072813,
             0.23734374636777902,
             1.5797862441444182e-10,
             0.1277124184014282]}

{'clustering': False,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.70987768,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.70988641,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.70987236,  5.        ]]),
 'entropy': True,
 'mcda': 'PROMETHEEII',
 'ml': 'MLPClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.40'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.23'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '-0.63'}],
 'weights': [1.1589190107833798e-15,
             0.634943835072813,
             0.23734374636777902,
             1.5797862441444182e-10,
             0.1277124184014282]}


HLCTH CLUSTERING STRATEGY with Polymer and Weights by Entropy
=============================================================

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': True,
 'mcda': 'TOPSIS',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.90'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.77'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.15'}],
 'weights': [1.158919010966463e-15,
             0.6349438351731198,
             0.237343746405274,
             1.158919010966463e-15,
             0.12771241842160388]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': True,
 'mcda': 'VIKOR',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '1.00'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.81'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.00'}],
 'weights': [1.158919010966463e-15,
             0.6349438351731198,
             0.237343746405274,
             1.158919010966463e-15,
             0.12771241842160388]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': True,
 'mcda': 'WASPAS',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '3.66'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '2.67'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '2.30'}],
 'weights': [1.158919010966463e-15,
             0.6349438351731198,
             0.237343746405274,
             1.158919010966463e-15,
             0.12771241842160388]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': True,
 'mcda': 'ELECTREII',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.75'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '0.50'}],
 'weights': [1.158919010966463e-15,
             0.6349438351731198,
             0.237343746405274,
             1.158919010966463e-15,
             0.12771241842160388]}

{'clustering': True,
 'criteria': array([[ 1.        ,  4.        ,  8.        ,  0.99844479,  7.        ],
                    [ 1.        ,  4.        , 18.        ,  0.99844479,  4.        ],
                    [ 1.        ,  1.        , 14.        ,  0.99844479,  5.        ]]),
 'entropy': True,
 'mcda': 'PROMETHEEII',
 'ml': 'RandomForestClassifier',
 'ranking': [{'chain_id': 'attack-chaining--0f1283e4600794736e61b11126589a3b',
              'weigth': '0.40'},
             {'chain_id': 'attack-chaining--6821aa25ad4e38227f2bc991fd014bc4',
              'weigth': '0.23'},
             {'chain_id': 'attack-chaining--8be7a62f94f84111854cb34adad5e514',
              'weigth': '-0.63'}],
 'weights': [1.158919010966463e-15,
             0.6349438351731198,
             0.237343746405274,
             1.158919010966463e-15,
             0.12771241842160388]}

"""