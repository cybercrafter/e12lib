from scikitmcda.ensemble import ENSEMBLE 
from tabulate import tabulate
from e12lib.priority import Priority

# Test of severity
threats_topsis = Priority()
threats_waspas = Priority()
threats_electre_ii = Priority()

threat_series_severity = [["CVE-2020-6316",  "CVE-2020-6268",
                           "CVE-2020-26807", "CVE-2019-0325",
                           "CVE-2020-8983", "CVE-2019-18225"], # 1. ERP
                          ["CVE-2020-8983", "CVE-2019-18225"], # 2. Remote Access
                          ["CVE-2021-2113"],                   # 3. Billing
                          ["CVE-2019-6183",  "CVE-2017-0144"], # 4. Workstation
                          ["CVE-2020-4383",  "CVE-2020-4382",
                           "CVE-2020-8983", "CVE-2019-18225"]]  # 5. Storage

alternatives = ["ERP", "Remote Access", "Billing", "Workstation", "Storage"]

# Setting Impact by AHP    
        # ERP,  RA,   Bil, Wrk, Stg 
i_AHP = [[  1,    6,    5,   2,    3],   # ERP
         [1/6,    1,  1/4, 1/3,  1/2],   # Remote Access
         [1/5,    4,    1, 1/4,  1/2],   # Billing
         [1/2,    3,    4,   1,    3],   # Workstation 
         [1/3,    2,    2, 1/3,    1]]   # Storage

# Ranking TOPSIS
threats_topsis.ranking_by_chain_risk(threat_series_severity, alt_labels=alternatives, impact_ahp=i_AHP)
print(tabulate(threats_topsis.df_decision, threats_topsis.df_decision.columns, tablefmt="psql"))

# Ranking with WASPAS
threats_waspas.ranking_by_chain_risk(threat_series_severity, mcda="WASPAS", alt_labels=alternatives, impact_ahp=i_AHP)
print(tabulate(threats_waspas.df_decision, threats_waspas.df_decision.columns, tablefmt="psql"))

# Ranking with ELECTRE II
threats_electre_ii.ranking_by_chain_risk(threat_series_severity, mcda="ELECTRE_II", alt_labels=alternatives, impact_ahp=i_AHP)
print(tabulate(threats_electre_ii.df_decision, threats_electre_ii.df_decision.columns, tablefmt="psql"))

# Ranking ENSEMBLE
threats_voting_soft = ENSEMBLE()
threats_voting_soft.ranking_by_voting([threats_topsis.method, threats_waspas.method, threats_electre_ii.method])
print(tabulate(threats_voting_soft.df_decision, threats_voting_soft.df_decision.columns, tablefmt="psql"))
