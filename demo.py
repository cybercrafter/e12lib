from e12lib.kdd import KDDSec
from e12lib.hlcth import Screener
from e12lib.killchain import Polymer, Link, Chain
import pprint
from tabulate import tabulate
from e12lib.priority import Priority

kddsec = KDDSec()
knowledge = kddsec.load()

print("ATT&CK Version: ", knowledge.attack_version)
print("Tactics: ", len(knowledge.tactics))
print("Techniques (distinct): ", len(knowledge.techniques))
print("Mitigations: ", len(knowledge.mitigations))
print("Groups: ", len(knowledge.groups))
print("Malwares: ", len(knowledge.malwares))
print("Tools: ", len(knowledge.tools))
print("Procedures: ", len(knowledge.procedures))
print("Relationships: ", len(knowledge.relationships))
print("Binary Table:\n", knowledge.binary_table)

polymer = Polymer(knowledge)

print("\n\nPolymer Version: ", polymer.version)
print("ATT&CK Version: ", polymer.attack_version)
print("Platforms: ", len(polymer.platforms))
print("Techniques (not unique): ", len(polymer.techniques))
print("Polymer phases: ", len(polymer.phases))
print("Binary Table (shape): ", polymer.binary_table.shape)

screener = Screener(polymer)
model_fit = screener.training(False, "GradientBoostingClassifier")

print("\n\nModel\n")
pprint.pprint(model_fit)

# Strider
threat = []
threat.append(Link(polymer, "T1090.001", "Android"))
threat.append(Link(polymer, "T1564.005", "Linux"))
threat.append(Link(polymer, "T1556.002", "Windows"))
threat_chaining_1 = Chain(threat)

# PunchTrack
threat = []
threat.append(Link(polymer, "CAPEC-267", "AWS"))
threat.append(Link(polymer, "T1027", "Data Historian"))
threat.append(Link(polymer, "T1005", "Windows"))
threat.append(Link(polymer, "T1074.001", "Windows"))
threat_chaining_2 = Chain(threat)

# ASPXSpy
threat = []
threat.append(Link(polymer, "CAPEC-650", "AWS"))
threat.append(Link(polymer, "T1505.003", "Data Historian"))
threat_chaining_3 = Chain(threat)

print("\nStrider is ", threat_chaining_1.chain_id)
print("PunchTrack is ", threat_chaining_2.chain_id)
print("ASPXSpy is ", threat_chaining_3.chain_id)


threat_series = [threat_chaining_1, threat_chaining_2, threat_chaining_3]
ranking = screener.rank(threat_series, model_fit)
print("\n\nHLCTH with 5 Criteria\n") 
pprint.pprint(ranking)

threat_series_severity = [["CVE-2017-7269", "CVE-2014-6352"], # CVEs threat_chaining_1
                          ["CVE-2012-0158", "CVE-2017-0199"], # CVEs threat_chaining_2
                          ["CVE-2014-4114", "CVE-2018-0798"]] # CVEs threat_chaining_3

ranking_with_severity = screener.rank_with_severity(threat_series, threat_series_severity, model_fit, ensemble_severity=True)
print("\nHLCTH with 6 Criteria(Severity) \n")
pprint.pprint(ranking_with_severity)

# Test of prioritizing by severity
severity = Priority()
severity.ranking_by_chain_severity(threat_series_severity)
print("RANKING BY SEVERITY:\n", tabulate(severity.df_decision, severity.df_decision.columns, tablefmt="psql"))

# Test of Risk
risk = Priority()
risk.ranking_by_chain_risk(threat_series_severity)
print("RANKING BY RISK:\n", tabulate(risk.df_decision, severity.df_decision.columns, tablefmt="psql"))
